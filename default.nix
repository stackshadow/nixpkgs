# { nixpkgs ? import <nixpkgs> { } # For nixpkgs dependencies used by NUR itself
#   # Dependencies to call NUR repos with
# , pkgs ? null, repoOverrides ? { } }:

# let
#   callPackage = nixpkgs.lib.callPackageWith;

#   syncthing = callPackage ./pkgs/syncthing/derivation.nix {  };

# in {
#   syncthing = syncthing {};
# }

{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { inherit system; };

  callPackage = pkgs.lib.callPackageWith (pkgs // self);
  syncthingPackages = import ./pkgs/syncthing/default.nix { };

  self = {
    syncthing_1_16_0 = syncthingPackages.v1_16_0;
    syncthing = syncthingPackages.latest;
    gocyclo = import ./pkgs/gocyclo/default.nix { };
    code-server-bin = callPackage ./pkgs/code-server-bin/default.nix { };
  };
in self
