{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.services.code-server;

  derivation = (pkgs.callPackage ../../pkgs/code-server-npm/derivation.nix { });

in {
  ###### interface
  options = {
    services.code-server = {
      enable = mkEnableOption "telegraf server";

      package = mkOption {
        default = derivation;
        defaultText = "pkgs.code-server-npm";
        description = "Which code-server derivation to use";
        type = types.package;
      };

      packages = mkOption {
        default = [ ];
        defaultText = "[]";
        description = "Packages that are available in the PATH of code-server";
        example = "[ pkgs.go ]";

      };

      host = mkOption {
        default = "127.0.0.1";
        defaultText = "Locahost-IP";
        description = "The host-ip to bind to";
        type = types.str;
      };

      port = mkOption {
        default = "4444";
        defaultText = "4444";
        description = "The port where code-server runs";
        type = types.str;
      };

      auth = mkOption {
        default = "password";
        defaultText = "Use password";
        description = "The type of authentication to use. [password, none]";
        type = types.str;
      };

      user = mkOption {
        default = "code-server";
        defaultText = "The internal code-server user";
        description = "Set the username for code-server";
        type = types.str;
      };

      groups = mkOption {
        default = "[]";
        defaultText = "[]";
        description = "An array of additional groups for the code-server user";
        example = ''[ "docker" ]'';
        type = types.listOf types.str;
      };

    };
  };

  ###### implementation
  config = mkIf config.services.code-server.enable {
    systemd.services.code-server = let

    in {
      description = "VSCode server";
      wantedBy = [ "multi-user.target" ];
      after = [ "network-online.target" ];
      path = cfg.packages;
      #environment = { PATH = "${pkgs.nixfmt}/bin"; LANG = "nl_NL.UTF-8";}; 
      serviceConfig = {
        ExecStart =
          #    "${pkgs.nodejs}/bin/node ${cfg.package}/opt/code-server/node_modules/.bin/code-server --disable-telemetry --bind-addr ${cfg.host}:${cfg.port} --auth ${cfg.auth}";
          "${cfg.package}/bin/code-server --disable-telemetry --bind-addr ${cfg.host}:${cfg.port} --auth ${cfg.auth}";
        ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
        RuntimeDirectory = "${cfg.package}";
        User = "${cfg.user}";
        Restart = "on-failure";
        # for ping probes
        AmbientCapabilities = [ "CAP_NET_RAW" ];

      };

    };

    users.users.code-server =
      mkIf (config.services.code-server.user == "code-server") {
        isNormalUser = true;
        createHome = true;
        description = "code-server user";
        home = "/home/code-server";
        extraGroups = cfg.groups;
        shell = pkgs.zsh;
      };
  };
}
