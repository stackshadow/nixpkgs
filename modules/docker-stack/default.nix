{ config, pkgs, lib, ... }:
let
  inherit (lib) mkDefault mkEnableOption mkForce mkIf mkMerge mkOption types;
  inherit (lib) mapAttrs mapAttrs' mapAttrsToList nameValuePair optional optionalAttrs optionalString;

  stacks = config.docker-stack;

  stackOptions = { ... }: {

    options = {

      # Enabled ?
      enable = lib.mkEnableOption "Enable stack";

      dockerImage = lib.mkOption {
        default = "";
        defaultText = "null";
        description = "An docker image that will be imported";
      };

      compose = lib.mkOption {
        default = "";
        defaultText = "";
        description = "Compose file as text";
      };
    };
  };

in {

  # interface
  options = {
    docker-stack = mkOption {
      type = types.attrsOf (types.submodule stackOptions);
      default = {};
      description = "One or more docker-stacks";
    };
  };

  ###### implementation
  config = mkIf (stacks != { }) {

    systemd.services = mkMerge [

      # this maps every element 
      # where key -> stackName
      # value -> cfg
      (mapAttrs' (stackName: cfg:
        (nameValuePair "docker-stack-${stackName}" {

          description = "Deploy ${stackName} stack";
          wantedBy = [ "multi-user.target" ];
          after = [ ];
          requires = [ ];

          environment = {
            DOCKER_HOST = "${config.environment.variables.DOCKER_HOST}";
          };

          script = ''
            if [ "${cfg.dockerImage}" != "" ]; then
              ${pkgs.docker}/bin/docker load < "${cfg.dockerImage}"
            fi
            ${pkgs.docker}/bin/docker stack deploy -c ${cfg.compose} ${stackName}
          '';

          postStop = ''
            ${pkgs.docker}/bin/docker stack rm ${stackName}
          '';

          serviceConfig = {
            Type = "oneshot";
            RemainAfterExit = true;
          };

        })) stacks)

    ];

  };

}
