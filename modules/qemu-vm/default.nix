{ config, kvmName ? "vm1" }:

let
  pkgs = import <nixpkgs> { };
  lib = pkgs.lib;
  cfg = config.qemuvm."${kvmName}";

in {
  ###### interface
  options = {
    qemuvm."${kvmName}" = {
      enable = lib.mkEnableOption "Start ${kvmName}";

      xmlFile = lib.mkOption {
        default = "";
        defaultText = "";
        description = "The config-xml for the vm";
      };

      configFile = lib.mkOption {
        default = null;
        defaultText = "no config";
        description = "Nixos configuration file";
      };

      kvmImage = lib.mkOption {
        default = import ../../pkgs/qimage/qimage.nix {
          additionalConfig = cfg.configFile;
        };
        defaultText = "null";
        description = "The image to use on the vm";
      };

    };
  };

  ###### implementation
  config = lib.mkIf cfg.enable {

    systemd.services."kvm-${kvmName}" = let

    in {
      after = [ "libvirtd.service" ];
      requires = [ "libvirtd.service" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
      };
      script = let
        xmlSource = [ (builtins.readFile cfg.xmlFile) ];
        xmlConfig = pkgs.writeTextFile {
          name = "${kvmName}.xml";
          text = xmlSource;
        };

      in ''
        # We need some vars
        export kvmName="${kvmName}"

        # We create the image file
        if case "${cfg.kvmImage}" in /nix/*) ;; *) false;; esac; then
          if [ ! -f /opt/kvm${cfg.kvmImage}/nixos.raw ]; then
            mkdir -p /opt/kvm${cfg.kvmImage}
            ${pkgs.qemu}/bin/qemu-img convert \
            ${cfg.kvmImage}/nixos.qcow2 /opt/kvm${cfg.kvmImage}/nixos.raw
          fi
          export kvmImage="/opt/kvm${cfg.kvmImage}/nixos.raw"
        else
          export kvmImage="${cfg.kvmImage}"
        fi
 
        #uuid="$(${pkgs.libvirt}/bin/virsh domuuid '${kvmName}' || true)"
        #${pkgs.libvirt}/bin/virsh define <(sed "s/UUID/$uuid/" '${xmlConfig}')
        #${pkgs.libvirt}/bin/virsh define ${xmlConfig}

        cat ${xmlConfig} | ${pkgs.envsubst}/bin/envsubst > /tmp/kvm-${kvmName}.xml
        ${pkgs.libvirt}/bin/virsh define <(cat ${xmlConfig} | ${pkgs.envsubst}/bin/envsubst )
        ${pkgs.libvirt}/bin/virsh start '${kvmName}'
      '';
      preStop = ''
        ${pkgs.libvirt}/bin/virsh shutdown '${kvmName}'
        let "timeout = $(date +%s) + 10"
        while [ "$(${pkgs.libvirt}/bin/virsh list --name | grep --count '^${kvmName}$')" -gt 0 ]; do
          if [ "$(date +%s)" -ge "$timeout" ]; then
            # Meh, we warned it...
            ${pkgs.libvirt}/bin/virsh destroy '${kvmName}'
          else
            # The machine is still running, let's give it some time to shut down
            sleep 0.5
          fi
        done
      '';
    };


  };
}
