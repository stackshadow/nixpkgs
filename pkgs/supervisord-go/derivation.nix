{ system ? builtins.currentSystem }:
let
  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;
  inherit (pkgs) buildGoModule fetchFromGitHub;

  common = { version, sha256, vendorSha256 }:
    buildGoModule rec {
      pname = "supervisord-go";
      inherit version;

      excludedPackages = "test";
      subPackages = [ "." ];

      # -> % nix-prefetch-url --unpack https://github.com/ochinchina/supervisord/archive/refs/tags/v0.7.3.tar.gz
      src = fetchFromGitHub {
        owner = "ochinchina";
        repo = "supervisord";
        rev = "refs/tags/v${version}";
        inherit sha256;
      };

      inherit vendorSha256;

      preBuild = ''
        buildFlagsArray+=( "-ldflags=-w -s -X main.version=${version}")
      '';

      #passthru.tests = { inherit (nixosTests) telegraf; };

      meta = with lib; {
        description = "re-implement of supervisord in go-lang";
        license = licenses.mit;
        homepage = "https://github.com/ochinchina/supervisord";
        maintainers = with maintainers; [ mic92 roblabla timstott foxit64 ];
      };
    };
in {
  latest = common {
    version = "0.7.3";
    sha256 = "0y5a6kaixm2hcjph5np52raspfd1azik1nhacjkfr3svnhfgrm2f";
    vendorSha256 =
      "sha256:1mc5wcg4gpjf1aghmnqams0iyp3spfq4359ziilsk49f05nn1ppz";
  };
}
