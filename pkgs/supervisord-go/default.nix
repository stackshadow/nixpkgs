{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { inherit system; };

  callPackage = pkgs.lib.callPackageWith (pkgs // self);
  supervisordPackage = import ./derivation.nix{};

  self = {
    latest = supervisordPackage.latest;
  };

in self
