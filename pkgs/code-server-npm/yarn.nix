{ fetchurl, fetchgit, linkFarm, runCommandNoCC, gnutar }: rec {
  offline_cache = linkFarm "offline" packages;
  packages = [
    {
      name = "_coder_logger___logger_1.1.16.tgz";
      path = fetchurl {
        name = "_coder_logger___logger_1.1.16.tgz";
        url  = "https://registry.yarnpkg.com/@coder/logger/-/logger-1.1.16.tgz";
        sha1 = "ee5b1b188f680733f35c11b065bbd139d618c1e1";
      };
    }
    {
      name = "_tootallnate_once___once_1.1.2.tgz";
      path = fetchurl {
        name = "_tootallnate_once___once_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/@tootallnate/once/-/once-1.1.2.tgz";
        sha1 = "ccb91445360179a04e7fe6aff78c00ffc1eeaf82";
      };
    }
    {
      name = "accepts___accepts_1.3.7.tgz";
      path = fetchurl {
        name = "accepts___accepts_1.3.7.tgz";
        url  = "https://registry.yarnpkg.com/accepts/-/accepts-1.3.7.tgz";
        sha1 = "531bc726517a3b2b41f850021c6cc15eaab507cd";
      };
    }
    {
      name = "agent_base___agent_base_6.0.2.tgz";
      path = fetchurl {
        name = "agent_base___agent_base_6.0.2.tgz";
        url  = "https://registry.yarnpkg.com/agent-base/-/agent-base-6.0.2.tgz";
        sha1 = "49fff58577cfee3f37176feab4c22e00f86d7f77";
      };
    }
    {
      name = "argparse___argparse_2.0.1.tgz";
      path = fetchurl {
        name = "argparse___argparse_2.0.1.tgz";
        url  = "https://registry.yarnpkg.com/argparse/-/argparse-2.0.1.tgz";
        sha1 = "246f50f3ca78a3240f6c997e8a9bd1eac49e4b38";
      };
    }
    {
      name = "array_flatten___array_flatten_2.1.1.tgz";
      path = fetchurl {
        name = "array_flatten___array_flatten_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/array-flatten/-/array-flatten-2.1.1.tgz";
        sha1 = "426bb9da84090c1838d812c8150af20a8331e296";
      };
    }
    {
      name = "ast_types___ast_types_0.13.4.tgz";
      path = fetchurl {
        name = "ast_types___ast_types_0.13.4.tgz";
        url  = "https://registry.yarnpkg.com/ast-types/-/ast-types-0.13.4.tgz";
        sha1 = "ee0d77b343263965ecc3fb62da16e7222b2b6782";
      };
    }
    {
      name = "balanced_match___balanced_match_1.0.2.tgz";
      path = fetchurl {
        name = "balanced_match___balanced_match_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/balanced-match/-/balanced-match-1.0.2.tgz";
        sha1 = "e83e3a7e3f300b34cb9d87f615fa0cbf357690ee";
      };
    }
    {
      name = "base64_js___base64_js_1.5.1.tgz";
      path = fetchurl {
        name = "base64_js___base64_js_1.5.1.tgz";
        url  = "https://registry.yarnpkg.com/base64-js/-/base64-js-1.5.1.tgz";
        sha1 = "1b1b440160a5bf7ad40b650f095963481903930a";
      };
    }
    {
      name = "bl___bl_4.1.0.tgz";
      path = fetchurl {
        name = "bl___bl_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/bl/-/bl-4.1.0.tgz";
        sha1 = "451535264182bec2fbbc83a62ab98cf11d9f7b3a";
      };
    }
    {
      name = "body_parser___body_parser_1.19.0.tgz";
      path = fetchurl {
        name = "body_parser___body_parser_1.19.0.tgz";
        url  = "https://registry.yarnpkg.com/body-parser/-/body-parser-1.19.0.tgz";
        sha1 = "96b2709e57c9c4e09a6fd66a8fd979844f69f08a";
      };
    }
    {
      name = "brace_expansion___brace_expansion_1.1.11.tgz";
      path = fetchurl {
        name = "brace_expansion___brace_expansion_1.1.11.tgz";
        url  = "https://registry.yarnpkg.com/brace-expansion/-/brace-expansion-1.1.11.tgz";
        sha1 = "3c7fcbf529d87226f3d2f52b966ff5271eb441dd";
      };
    }
    {
      name = "buffer_alloc_unsafe___buffer_alloc_unsafe_1.1.0.tgz";
      path = fetchurl {
        name = "buffer_alloc_unsafe___buffer_alloc_unsafe_1.1.0.tgz";
        url  = "https://registry.yarnpkg.com/buffer-alloc-unsafe/-/buffer-alloc-unsafe-1.1.0.tgz";
        sha1 = "bd7dc26ae2972d0eda253be061dba992349c19f0";
      };
    }
    {
      name = "buffer_alloc___buffer_alloc_1.2.0.tgz";
      path = fetchurl {
        name = "buffer_alloc___buffer_alloc_1.2.0.tgz";
        url  = "https://registry.yarnpkg.com/buffer-alloc/-/buffer-alloc-1.2.0.tgz";
        sha1 = "890dd90d923a873e08e10e5fd51a57e5b7cce0ec";
      };
    }
    {
      name = "buffer_fill___buffer_fill_1.0.0.tgz";
      path = fetchurl {
        name = "buffer_fill___buffer_fill_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/buffer-fill/-/buffer-fill-1.0.0.tgz";
        sha1 = "f8f78b76789888ef39f205cd637f68e702122b2c";
      };
    }
    {
      name = "buffer___buffer_5.7.1.tgz";
      path = fetchurl {
        name = "buffer___buffer_5.7.1.tgz";
        url  = "https://registry.yarnpkg.com/buffer/-/buffer-5.7.1.tgz";
        sha1 = "ba62e7c13133053582197160851a8f648e99eed0";
      };
    }
    {
      name = "bytes___bytes_3.0.0.tgz";
      path = fetchurl {
        name = "bytes___bytes_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/bytes/-/bytes-3.0.0.tgz";
        sha1 = "d32815404d689699f85a4ea4fa8755dd13a96048";
      };
    }
    {
      name = "bytes___bytes_3.1.0.tgz";
      path = fetchurl {
        name = "bytes___bytes_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/bytes/-/bytes-3.1.0.tgz";
        sha1 = "f6cf7933a360e0588fa9fde85651cdc7f805d1f6";
      };
    }
    {
      name = "charenc___charenc_0.0.2.tgz";
      path = fetchurl {
        name = "charenc___charenc_0.0.2.tgz";
        url  = "https://registry.yarnpkg.com/charenc/-/charenc-0.0.2.tgz";
        sha1 = "c0a1d2f3a7092e03774bfa83f14c0fc5790a8667";
      };
    }
    {
      name = "chownr___chownr_1.1.4.tgz";
      path = fetchurl {
        name = "chownr___chownr_1.1.4.tgz";
        url  = "https://registry.yarnpkg.com/chownr/-/chownr-1.1.4.tgz";
        sha1 = "6fc9d7b42d32a583596337666e7d08084da2cc6b";
      };
    }
    {
      name = "cli___cli_1.0.1.tgz";
      path = fetchurl {
        name = "cli___cli_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/cli/-/cli-1.0.1.tgz";
        sha1 = "22817534f24bfa4950c34d532d48ecbc621b8c14";
      };
    }
    {
      name = "code_server___code_server_3.10.2.tgz";
      path = fetchurl {
        name = "code_server___code_server_3.10.2.tgz";
        url  = "https://registry.yarnpkg.com/code-server/-/code-server-3.10.2.tgz";
        sha1 = "4ce21ad8512091b19c0c10f5b698a83b3c4d9a1c";
      };
    }
    {
      name = "compressible___compressible_2.0.18.tgz";
      path = fetchurl {
        name = "compressible___compressible_2.0.18.tgz";
        url  = "https://registry.yarnpkg.com/compressible/-/compressible-2.0.18.tgz";
        sha1 = "af53cca6b070d4c3c0750fbd77286a6d7cc46fba";
      };
    }
    {
      name = "compression___compression_1.7.4.tgz";
      path = fetchurl {
        name = "compression___compression_1.7.4.tgz";
        url  = "https://registry.yarnpkg.com/compression/-/compression-1.7.4.tgz";
        sha1 = "95523eff170ca57c29a0ca41e6fe131f41e5bb8f";
      };
    }
    {
      name = "concat_map___concat_map_0.0.1.tgz";
      path = fetchurl {
        name = "concat_map___concat_map_0.0.1.tgz";
        url  = "https://registry.yarnpkg.com/concat-map/-/concat-map-0.0.1.tgz";
        sha1 = "d8a96bd77fd68df7793a73036a3ba0d5405d477b";
      };
    }
    {
      name = "content_disposition___content_disposition_0.5.3.tgz";
      path = fetchurl {
        name = "content_disposition___content_disposition_0.5.3.tgz";
        url  = "https://registry.yarnpkg.com/content-disposition/-/content-disposition-0.5.3.tgz";
        sha1 = "e130caf7e7279087c5616c2007d0485698984fbd";
      };
    }
    {
      name = "content_type___content_type_1.0.4.tgz";
      path = fetchurl {
        name = "content_type___content_type_1.0.4.tgz";
        url  = "https://registry.yarnpkg.com/content-type/-/content-type-1.0.4.tgz";
        sha1 = "e138cc75e040c727b1966fe5e5f8c9aee256fe3b";
      };
    }
    {
      name = "cookie_parser___cookie_parser_1.4.5.tgz";
      path = fetchurl {
        name = "cookie_parser___cookie_parser_1.4.5.tgz";
        url  = "https://registry.yarnpkg.com/cookie-parser/-/cookie-parser-1.4.5.tgz";
        sha1 = "3e572d4b7c0c80f9c61daf604e4336831b5d1d49";
      };
    }
    {
      name = "cookie_signature___cookie_signature_1.0.6.tgz";
      path = fetchurl {
        name = "cookie_signature___cookie_signature_1.0.6.tgz";
        url  = "https://registry.yarnpkg.com/cookie-signature/-/cookie-signature-1.0.6.tgz";
        sha1 = "e303a882b342cc3ee8ca513a79999734dab3ae2c";
      };
    }
    {
      name = "cookie___cookie_0.4.0.tgz";
      path = fetchurl {
        name = "cookie___cookie_0.4.0.tgz";
        url  = "https://registry.yarnpkg.com/cookie/-/cookie-0.4.0.tgz";
        sha1 = "beb437e7022b3b6d49019d088665303ebe9c14ba";
      };
    }
    {
      name = "core_util_is___core_util_is_1.0.2.tgz";
      path = fetchurl {
        name = "core_util_is___core_util_is_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/core-util-is/-/core-util-is-1.0.2.tgz";
        sha1 = "b5fd54220aa2bc5ab57aab7140c940754503c1a7";
      };
    }
    {
      name = "crypt___crypt_0.0.2.tgz";
      path = fetchurl {
        name = "crypt___crypt_0.0.2.tgz";
        url  = "https://registry.yarnpkg.com/crypt/-/crypt-0.0.2.tgz";
        sha1 = "88d7ff7ec0dfb86f713dc87bbb42d044d3e6c41b";
      };
    }
    {
      name = "data_uri_to_buffer___data_uri_to_buffer_3.0.1.tgz";
      path = fetchurl {
        name = "data_uri_to_buffer___data_uri_to_buffer_3.0.1.tgz";
        url  = "https://registry.yarnpkg.com/data-uri-to-buffer/-/data-uri-to-buffer-3.0.1.tgz";
        sha1 = "594b8973938c5bc2c33046535785341abc4f3636";
      };
    }
    {
      name = "debug___debug_2.6.9.tgz";
      path = fetchurl {
        name = "debug___debug_2.6.9.tgz";
        url  = "https://registry.yarnpkg.com/debug/-/debug-2.6.9.tgz";
        sha1 = "5d128515df134ff327e90a4c93f4e077a536341f";
      };
    }
    {
      name = "debug___debug_3.1.0.tgz";
      path = fetchurl {
        name = "debug___debug_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/debug/-/debug-3.1.0.tgz";
        sha1 = "5bb5a0672628b64149566ba16819e61518c67261";
      };
    }
    {
      name = "debug___debug_4.3.1.tgz";
      path = fetchurl {
        name = "debug___debug_4.3.1.tgz";
        url  = "https://registry.yarnpkg.com/debug/-/debug-4.3.1.tgz";
        sha1 = "f0d229c505e0c6d8c49ac553d1b13dc183f6b2ee";
      };
    }
    {
      name = "deep_is___deep_is_0.1.3.tgz";
      path = fetchurl {
        name = "deep_is___deep_is_0.1.3.tgz";
        url  = "https://registry.yarnpkg.com/deep-is/-/deep-is-0.1.3.tgz";
        sha1 = "b369d6fb5dbc13eecf524f91b070feedc357cf34";
      };
    }
    {
      name = "degenerator___degenerator_2.2.0.tgz";
      path = fetchurl {
        name = "degenerator___degenerator_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/degenerator/-/degenerator-2.2.0.tgz";
        sha1 = "49e98c11fa0293c5b26edfbb52f15729afcdb254";
      };
    }
    {
      name = "depd___depd_1.1.2.tgz";
      path = fetchurl {
        name = "depd___depd_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/depd/-/depd-1.1.2.tgz";
        sha1 = "9bcd52e14c097763e749b274c4346ed2e560b5a9";
      };
    }
    {
      name = "destroy___destroy_1.0.4.tgz";
      path = fetchurl {
        name = "destroy___destroy_1.0.4.tgz";
        url  = "https://registry.yarnpkg.com/destroy/-/destroy-1.0.4.tgz";
        sha1 = "978857442c44749e4206613e37946205826abd80";
      };
    }
    {
      name = "ee_first___ee_first_1.1.1.tgz";
      path = fetchurl {
        name = "ee_first___ee_first_1.1.1.tgz";
        url  = "https://registry.yarnpkg.com/ee-first/-/ee-first-1.1.1.tgz";
        sha1 = "590c61156b0ae2f4f0255732a158b266bc56b21d";
      };
    }
    {
      name = "encodeurl___encodeurl_1.0.2.tgz";
      path = fetchurl {
        name = "encodeurl___encodeurl_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/encodeurl/-/encodeurl-1.0.2.tgz";
        sha1 = "ad3ff4c86ec2d029322f5a02c3a9a606c95b3f59";
      };
    }
    {
      name = "end_of_stream___end_of_stream_1.4.4.tgz";
      path = fetchurl {
        name = "end_of_stream___end_of_stream_1.4.4.tgz";
        url  = "https://registry.yarnpkg.com/end-of-stream/-/end-of-stream-1.4.4.tgz";
        sha1 = "5ae64a5f45057baf3626ec14da0ca5e4b2431eb0";
      };
    }
    {
      name = "env_paths___env_paths_2.2.1.tgz";
      path = fetchurl {
        name = "env_paths___env_paths_2.2.1.tgz";
        url  = "https://registry.yarnpkg.com/env-paths/-/env-paths-2.2.1.tgz";
        sha1 = "420399d416ce1fbe9bc0a07c62fa68d67fd0f8f2";
      };
    }
    {
      name = "es6_promisify___es6_promisify_6.1.1.tgz";
      path = fetchurl {
        name = "es6_promisify___es6_promisify_6.1.1.tgz";
        url  = "https://registry.yarnpkg.com/es6-promisify/-/es6-promisify-6.1.1.tgz";
        sha1 = "46837651b7b06bf6fff893d03f29393668d01621";
      };
    }
    {
      name = "escape_html___escape_html_1.0.3.tgz";
      path = fetchurl {
        name = "escape_html___escape_html_1.0.3.tgz";
        url  = "https://registry.yarnpkg.com/escape-html/-/escape-html-1.0.3.tgz";
        sha1 = "0258eae4d3d0c0974de1c169188ef0051d1d1988";
      };
    }
    {
      name = "escodegen___escodegen_1.14.3.tgz";
      path = fetchurl {
        name = "escodegen___escodegen_1.14.3.tgz";
        url  = "https://registry.yarnpkg.com/escodegen/-/escodegen-1.14.3.tgz";
        sha1 = "4e7b81fba61581dc97582ed78cab7f0e8d63f503";
      };
    }
    {
      name = "esprima___esprima_4.0.1.tgz";
      path = fetchurl {
        name = "esprima___esprima_4.0.1.tgz";
        url  = "https://registry.yarnpkg.com/esprima/-/esprima-4.0.1.tgz";
        sha1 = "13b04cdb3e6c5d19df91ab6987a8695619b0aa71";
      };
    }
    {
      name = "estraverse___estraverse_4.3.0.tgz";
      path = fetchurl {
        name = "estraverse___estraverse_4.3.0.tgz";
        url  = "https://registry.yarnpkg.com/estraverse/-/estraverse-4.3.0.tgz";
        sha1 = "398ad3f3c5a24948be7725e83d11a7de28cdbd1d";
      };
    }
    {
      name = "esutils___esutils_2.0.3.tgz";
      path = fetchurl {
        name = "esutils___esutils_2.0.3.tgz";
        url  = "https://registry.yarnpkg.com/esutils/-/esutils-2.0.3.tgz";
        sha1 = "74d2eb4de0b8da1293711910d50775b9b710ef64";
      };
    }
    {
      name = "etag___etag_1.8.1.tgz";
      path = fetchurl {
        name = "etag___etag_1.8.1.tgz";
        url  = "https://registry.yarnpkg.com/etag/-/etag-1.8.1.tgz";
        sha1 = "41ae2eeb65efa62268aebfea83ac7d79299b0887";
      };
    }
    {
      name = "eventemitter3___eventemitter3_4.0.7.tgz";
      path = fetchurl {
        name = "eventemitter3___eventemitter3_4.0.7.tgz";
        url  = "https://registry.yarnpkg.com/eventemitter3/-/eventemitter3-4.0.7.tgz";
        sha1 = "2de9b68f6528d5644ef5c59526a1b4a07306169f";
      };
    }
    {
      name = "exit___exit_0.1.2.tgz";
      path = fetchurl {
        name = "exit___exit_0.1.2.tgz";
        url  = "https://registry.yarnpkg.com/exit/-/exit-0.1.2.tgz";
        sha1 = "0632638f8d877cc82107d30a0fff1a17cba1cd0c";
      };
    }
    {
      name = "express___express_5.0.0_alpha.8.tgz";
      path = fetchurl {
        name = "express___express_5.0.0_alpha.8.tgz";
        url  = "https://registry.yarnpkg.com/express/-/express-5.0.0-alpha.8.tgz";
        sha1 = "b9dd3a568eab791e3391db47f9e6ab91e61b13fe";
      };
    }
    {
      name = "fast_levenshtein___fast_levenshtein_2.0.6.tgz";
      path = fetchurl {
        name = "fast_levenshtein___fast_levenshtein_2.0.6.tgz";
        url  = "https://registry.yarnpkg.com/fast-levenshtein/-/fast-levenshtein-2.0.6.tgz";
        sha1 = "3d8a5c66883a16a30ca8643e851f19baa7797917";
      };
    }
    {
      name = "file_uri_to_path___file_uri_to_path_2.0.0.tgz";
      path = fetchurl {
        name = "file_uri_to_path___file_uri_to_path_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/file-uri-to-path/-/file-uri-to-path-2.0.0.tgz";
        sha1 = "7b415aeba227d575851e0a5b0c640d7656403fba";
      };
    }
    {
      name = "finalhandler___finalhandler_1.1.2.tgz";
      path = fetchurl {
        name = "finalhandler___finalhandler_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/finalhandler/-/finalhandler-1.1.2.tgz";
        sha1 = "b7e7d000ffd11938d0fdb053506f6ebabe9f587d";
      };
    }
    {
      name = "follow_redirects___follow_redirects_1.14.1.tgz";
      path = fetchurl {
        name = "follow_redirects___follow_redirects_1.14.1.tgz";
        url  = "https://registry.yarnpkg.com/follow-redirects/-/follow-redirects-1.14.1.tgz";
        sha1 = "d9114ded0a1cfdd334e164e6662ad02bfd91ff43";
      };
    }
    {
      name = "forwarded___forwarded_0.1.2.tgz";
      path = fetchurl {
        name = "forwarded___forwarded_0.1.2.tgz";
        url  = "https://registry.yarnpkg.com/forwarded/-/forwarded-0.1.2.tgz";
        sha1 = "98c23dab1175657b8c0573e8ceccd91b0ff18c84";
      };
    }
    {
      name = "fresh___fresh_0.5.2.tgz";
      path = fetchurl {
        name = "fresh___fresh_0.5.2.tgz";
        url  = "https://registry.yarnpkg.com/fresh/-/fresh-0.5.2.tgz";
        sha1 = "3d8cadd90d976569fa835ab1f8e4b23a105605a7";
      };
    }
    {
      name = "fs_constants___fs_constants_1.0.0.tgz";
      path = fetchurl {
        name = "fs_constants___fs_constants_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/fs-constants/-/fs-constants-1.0.0.tgz";
        sha1 = "6be0de9be998ce16af8afc24497b9ee9b7ccd9ad";
      };
    }
    {
      name = "fs_extra___fs_extra_8.1.0.tgz";
      path = fetchurl {
        name = "fs_extra___fs_extra_8.1.0.tgz";
        url  = "https://registry.yarnpkg.com/fs-extra/-/fs-extra-8.1.0.tgz";
        sha1 = "49d43c45a88cd9677668cb7be1b46efdb8d2e1c0";
      };
    }
    {
      name = "fs.realpath___fs.realpath_1.0.0.tgz";
      path = fetchurl {
        name = "fs.realpath___fs.realpath_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/fs.realpath/-/fs.realpath-1.0.0.tgz";
        sha1 = "1504ad2523158caa40db4a2787cb01411994ea4f";
      };
    }
    {
      name = "ftp___ftp_0.3.10.tgz";
      path = fetchurl {
        name = "ftp___ftp_0.3.10.tgz";
        url  = "https://registry.yarnpkg.com/ftp/-/ftp-0.3.10.tgz";
        sha1 = "9197d861ad8142f3e63d5a83bfe4c59f7330885d";
      };
    }
    {
      name = "get_uri___get_uri_3.0.2.tgz";
      path = fetchurl {
        name = "get_uri___get_uri_3.0.2.tgz";
        url  = "https://registry.yarnpkg.com/get-uri/-/get-uri-3.0.2.tgz";
        sha1 = "f0ef1356faabc70e1f9404fa3b66b2ba9bfc725c";
      };
    }
    {
      name = "glob___glob_7.1.7.tgz";
      path = fetchurl {
        name = "glob___glob_7.1.7.tgz";
        url  = "https://registry.yarnpkg.com/glob/-/glob-7.1.7.tgz";
        sha1 = "3b193e9233f01d42d0b3f78294bbeeb418f94a90";
      };
    }
    {
      name = "graceful_fs___graceful_fs_4.2.6.tgz";
      path = fetchurl {
        name = "graceful_fs___graceful_fs_4.2.6.tgz";
        url  = "https://registry.yarnpkg.com/graceful-fs/-/graceful-fs-4.2.6.tgz";
        sha1 = "ff040b2b0853b23c3d31027523706f1885d76bee";
      };
    }
    {
      name = "http_errors___http_errors_1.7.2.tgz";
      path = fetchurl {
        name = "http_errors___http_errors_1.7.2.tgz";
        url  = "https://registry.yarnpkg.com/http-errors/-/http-errors-1.7.2.tgz";
        sha1 = "4f5029cf13239f31036e5b2e55292bcfbcc85c8f";
      };
    }
    {
      name = "http_errors___http_errors_1.7.3.tgz";
      path = fetchurl {
        name = "http_errors___http_errors_1.7.3.tgz";
        url  = "https://registry.yarnpkg.com/http-errors/-/http-errors-1.7.3.tgz";
        sha1 = "6c619e4f9c60308c38519498c14fbb10aacebb06";
      };
    }
    {
      name = "http_proxy_agent___http_proxy_agent_4.0.1.tgz";
      path = fetchurl {
        name = "http_proxy_agent___http_proxy_agent_4.0.1.tgz";
        url  = "https://registry.yarnpkg.com/http-proxy-agent/-/http-proxy-agent-4.0.1.tgz";
        sha1 = "8a8c8ef7f5932ccf953c296ca8291b95aa74aa3a";
      };
    }
    {
      name = "http_proxy___http_proxy_1.18.1.tgz";
      path = fetchurl {
        name = "http_proxy___http_proxy_1.18.1.tgz";
        url  = "https://registry.yarnpkg.com/http-proxy/-/http-proxy-1.18.1.tgz";
        sha1 = "401541f0534884bbf95260334e72f88ee3976549";
      };
    }
    {
      name = "httpolyglot___httpolyglot_0.1.2.tgz";
      path = fetchurl {
        name = "httpolyglot___httpolyglot_0.1.2.tgz";
        url  = "https://registry.yarnpkg.com/httpolyglot/-/httpolyglot-0.1.2.tgz";
        sha1 = "e4d347fe8984a62f467d4060df527f1851f6997b";
      };
    }
    {
      name = "https_proxy_agent___https_proxy_agent_5.0.0.tgz";
      path = fetchurl {
        name = "https_proxy_agent___https_proxy_agent_5.0.0.tgz";
        url  = "https://registry.yarnpkg.com/https-proxy-agent/-/https-proxy-agent-5.0.0.tgz";
        sha1 = "e2a90542abb68a762e0a0850f6c9edadfd8506b2";
      };
    }
    {
      name = "iconv_lite___iconv_lite_0.4.24.tgz";
      path = fetchurl {
        name = "iconv_lite___iconv_lite_0.4.24.tgz";
        url  = "https://registry.yarnpkg.com/iconv-lite/-/iconv-lite-0.4.24.tgz";
        sha1 = "2022b4b25fbddc21d2f524974a474aafe733908b";
      };
    }
    {
      name = "ieee754___ieee754_1.2.1.tgz";
      path = fetchurl {
        name = "ieee754___ieee754_1.2.1.tgz";
        url  = "https://registry.yarnpkg.com/ieee754/-/ieee754-1.2.1.tgz";
        sha1 = "8eb7a10a63fff25d15a57b001586d177d1b0d352";
      };
    }
    {
      name = "inflight___inflight_1.0.6.tgz";
      path = fetchurl {
        name = "inflight___inflight_1.0.6.tgz";
        url  = "https://registry.yarnpkg.com/inflight/-/inflight-1.0.6.tgz";
        sha1 = "49bd6331d7d02d0c09bc910a1075ba8165b56df9";
      };
    }
    {
      name = "inherits___inherits_2.0.4.tgz";
      path = fetchurl {
        name = "inherits___inherits_2.0.4.tgz";
        url  = "https://registry.yarnpkg.com/inherits/-/inherits-2.0.4.tgz";
        sha1 = "0fa2c64f932917c3433a0ded55363aae37416b7c";
      };
    }
    {
      name = "inherits___inherits_2.0.3.tgz";
      path = fetchurl {
        name = "inherits___inherits_2.0.3.tgz";
        url  = "https://registry.yarnpkg.com/inherits/-/inherits-2.0.3.tgz";
        sha1 = "633c2c83e3da42a502f52466022480f4208261de";
      };
    }
    {
      name = "ip___ip_1.1.5.tgz";
      path = fetchurl {
        name = "ip___ip_1.1.5.tgz";
        url  = "https://registry.yarnpkg.com/ip/-/ip-1.1.5.tgz";
        sha1 = "bdded70114290828c0a039e72ef25f5aaec4354a";
      };
    }
    {
      name = "ipaddr.js___ipaddr.js_1.9.1.tgz";
      path = fetchurl {
        name = "ipaddr.js___ipaddr.js_1.9.1.tgz";
        url  = "https://registry.yarnpkg.com/ipaddr.js/-/ipaddr.js-1.9.1.tgz";
        sha1 = "bff38543eeb8984825079ff3a2a8e6cbd46781b3";
      };
    }
    {
      name = "is_buffer___is_buffer_1.1.6.tgz";
      path = fetchurl {
        name = "is_buffer___is_buffer_1.1.6.tgz";
        url  = "https://registry.yarnpkg.com/is-buffer/-/is-buffer-1.1.6.tgz";
        sha1 = "efaa2ea9daa0d7ab2ea13a97b2b8ad51fefbe8be";
      };
    }
    {
      name = "isarray___isarray_0.0.1.tgz";
      path = fetchurl {
        name = "isarray___isarray_0.0.1.tgz";
        url  = "https://registry.yarnpkg.com/isarray/-/isarray-0.0.1.tgz";
        sha1 = "8a18acfca9a8f4177e09abfc6038939b05d1eedf";
      };
    }
    {
      name = "isexe___isexe_2.0.0.tgz";
      path = fetchurl {
        name = "isexe___isexe_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/isexe/-/isexe-2.0.0.tgz";
        sha1 = "e8fbf374dc556ff8947a10dcb0572d633f2cfa10";
      };
    }
    {
      name = "js_yaml___js_yaml_4.1.0.tgz";
      path = fetchurl {
        name = "js_yaml___js_yaml_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/js-yaml/-/js-yaml-4.1.0.tgz";
        sha1 = "c1fb65f8f5017901cdd2c951864ba18458a10602";
      };
    }
    {
      name = "jsonfile___jsonfile_4.0.0.tgz";
      path = fetchurl {
        name = "jsonfile___jsonfile_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/jsonfile/-/jsonfile-4.0.0.tgz";
        sha1 = "8771aae0799b64076b76640fca058f9c10e33ecb";
      };
    }
    {
      name = "levn___levn_0.3.0.tgz";
      path = fetchurl {
        name = "levn___levn_0.3.0.tgz";
        url  = "https://registry.yarnpkg.com/levn/-/levn-0.3.0.tgz";
        sha1 = "3b09924edf9f083c0490fdd4c0bc4421e04764ee";
      };
    }
    {
      name = "limiter___limiter_1.1.5.tgz";
      path = fetchurl {
        name = "limiter___limiter_1.1.5.tgz";
        url  = "https://registry.yarnpkg.com/limiter/-/limiter-1.1.5.tgz";
        sha1 = "8f92a25b3b16c6131293a0cc834b4a838a2aa7c2";
      };
    }
    {
      name = "lru_cache___lru_cache_5.1.1.tgz";
      path = fetchurl {
        name = "lru_cache___lru_cache_5.1.1.tgz";
        url  = "https://registry.yarnpkg.com/lru-cache/-/lru-cache-5.1.1.tgz";
        sha1 = "1da27e6710271947695daf6848e847f01d84b920";
      };
    }
    {
      name = "lru_cache___lru_cache_6.0.0.tgz";
      path = fetchurl {
        name = "lru_cache___lru_cache_6.0.0.tgz";
        url  = "https://registry.yarnpkg.com/lru-cache/-/lru-cache-6.0.0.tgz";
        sha1 = "6d6fe6570ebd96aaf90fcad1dafa3b2566db3a94";
      };
    }
    {
      name = "md5___md5_2.3.0.tgz";
      path = fetchurl {
        name = "md5___md5_2.3.0.tgz";
        url  = "https://registry.yarnpkg.com/md5/-/md5-2.3.0.tgz";
        sha1 = "c3da9a6aae3a30b46b7b0c349b87b110dc3bda4f";
      };
    }
    {
      name = "media_typer___media_typer_0.3.0.tgz";
      path = fetchurl {
        name = "media_typer___media_typer_0.3.0.tgz";
        url  = "https://registry.yarnpkg.com/media-typer/-/media-typer-0.3.0.tgz";
        sha1 = "8710d7af0aa626f8fffa1ce00168545263255748";
      };
    }
    {
      name = "merge_descriptors___merge_descriptors_1.0.1.tgz";
      path = fetchurl {
        name = "merge_descriptors___merge_descriptors_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/merge-descriptors/-/merge-descriptors-1.0.1.tgz";
        sha1 = "b00aaa556dd8b44568150ec9d1b953f3f90cbb61";
      };
    }
    {
      name = "methods___methods_1.1.2.tgz";
      path = fetchurl {
        name = "methods___methods_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/methods/-/methods-1.1.2.tgz";
        sha1 = "5529a4d67654134edcc5266656835b0f851afcee";
      };
    }
    {
      name = "mime_db___mime_db_1.47.0.tgz";
      path = fetchurl {
        name = "mime_db___mime_db_1.47.0.tgz";
        url  = "https://registry.yarnpkg.com/mime-db/-/mime-db-1.47.0.tgz";
        sha1 = "8cb313e59965d3c05cfbf898915a267af46a335c";
      };
    }
    {
      name = "mime_types___mime_types_2.1.30.tgz";
      path = fetchurl {
        name = "mime_types___mime_types_2.1.30.tgz";
        url  = "https://registry.yarnpkg.com/mime-types/-/mime-types-2.1.30.tgz";
        sha1 = "6e7be8b4c479825f85ed6326695db73f9305d62d";
      };
    }
    {
      name = "mime___mime_1.6.0.tgz";
      path = fetchurl {
        name = "mime___mime_1.6.0.tgz";
        url  = "https://registry.yarnpkg.com/mime/-/mime-1.6.0.tgz";
        sha1 = "32cd9e5c64553bd58d19a568af452acff04981b1";
      };
    }
    {
      name = "minimatch___minimatch_3.0.4.tgz";
      path = fetchurl {
        name = "minimatch___minimatch_3.0.4.tgz";
        url  = "https://registry.yarnpkg.com/minimatch/-/minimatch-3.0.4.tgz";
        sha1 = "5166e286457f03306064be5497e8dbb0c3d32083";
      };
    }
    {
      name = "mkdirp_classic___mkdirp_classic_0.5.3.tgz";
      path = fetchurl {
        name = "mkdirp_classic___mkdirp_classic_0.5.3.tgz";
        url  = "https://registry.yarnpkg.com/mkdirp-classic/-/mkdirp-classic-0.5.3.tgz";
        sha1 = "fa10c9115cc6d8865be221ba47ee9bed78601113";
      };
    }
    {
      name = "ms___ms_2.0.0.tgz";
      path = fetchurl {
        name = "ms___ms_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/ms/-/ms-2.0.0.tgz";
        sha1 = "5608aeadfc00be6c2901df5f9861788de0d597c8";
      };
    }
    {
      name = "ms___ms_2.1.1.tgz";
      path = fetchurl {
        name = "ms___ms_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/ms/-/ms-2.1.1.tgz";
        sha1 = "30a5864eb3ebb0a66f2ebe6d727af06a09d86e0a";
      };
    }
    {
      name = "ms___ms_2.1.2.tgz";
      path = fetchurl {
        name = "ms___ms_2.1.2.tgz";
        url  = "https://registry.yarnpkg.com/ms/-/ms-2.1.2.tgz";
        sha1 = "d09d1f357b443f493382a8eb3ccd183872ae6009";
      };
    }
    {
      name = "nan___nan_2.14.2.tgz";
      path = fetchurl {
        name = "nan___nan_2.14.2.tgz";
        url  = "https://registry.yarnpkg.com/nan/-/nan-2.14.2.tgz";
        sha1 = "f5376400695168f4cc694ac9393d0c9585eeea19";
      };
    }
    {
      name = "negotiator___negotiator_0.6.2.tgz";
      path = fetchurl {
        name = "negotiator___negotiator_0.6.2.tgz";
        url  = "https://registry.yarnpkg.com/negotiator/-/negotiator-0.6.2.tgz";
        sha1 = "feacf7ccf525a77ae9634436a64883ffeca346fb";
      };
    }
    {
      name = "netmask___netmask_2.0.2.tgz";
      path = fetchurl {
        name = "netmask___netmask_2.0.2.tgz";
        url  = "https://registry.yarnpkg.com/netmask/-/netmask-2.0.2.tgz";
        sha1 = "8b01a07644065d536383835823bc52004ebac5e7";
      };
    }
    {
      name = "node_fetch___node_fetch_2.6.1.tgz";
      path = fetchurl {
        name = "node_fetch___node_fetch_2.6.1.tgz";
        url  = "https://registry.yarnpkg.com/node-fetch/-/node-fetch-2.6.1.tgz";
        sha1 = "045bd323631f76ed2e2b55573394416b639a0052";
      };
    }
    {
      name = "node_pty___node_pty_0.10.1.tgz";
      path = fetchurl {
        name = "node_pty___node_pty_0.10.1.tgz";
        url  = "https://registry.yarnpkg.com/node-pty/-/node-pty-0.10.1.tgz";
        sha1 = "cd05d03a2710315ec40221232ec04186f6ac2c6d";
      };
    }
    {
      name = "on_finished___on_finished_2.3.0.tgz";
      path = fetchurl {
        name = "on_finished___on_finished_2.3.0.tgz";
        url  = "https://registry.yarnpkg.com/on-finished/-/on-finished-2.3.0.tgz";
        sha1 = "20f1336481b083cd75337992a16971aa2d906947";
      };
    }
    {
      name = "on_headers___on_headers_1.0.2.tgz";
      path = fetchurl {
        name = "on_headers___on_headers_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/on-headers/-/on-headers-1.0.2.tgz";
        sha1 = "772b0ae6aaa525c399e489adfad90c403eb3c28f";
      };
    }
    {
      name = "once___once_1.4.0.tgz";
      path = fetchurl {
        name = "once___once_1.4.0.tgz";
        url  = "https://registry.yarnpkg.com/once/-/once-1.4.0.tgz";
        sha1 = "583b1aa775961d4b113ac17d9c50baef9dd76bd1";
      };
    }
    {
      name = "optionator___optionator_0.8.3.tgz";
      path = fetchurl {
        name = "optionator___optionator_0.8.3.tgz";
        url  = "https://registry.yarnpkg.com/optionator/-/optionator-0.8.3.tgz";
        sha1 = "84fa1d036fe9d3c7e21d99884b601167ec8fb495";
      };
    }
    {
      name = "os_tmpdir___os_tmpdir_1.0.2.tgz";
      path = fetchurl {
        name = "os_tmpdir___os_tmpdir_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/os-tmpdir/-/os-tmpdir-1.0.2.tgz";
        sha1 = "bbe67406c79aa85c5cfec766fe5734555dfa1274";
      };
    }
    {
      name = "pac_proxy_agent___pac_proxy_agent_4.1.0.tgz";
      path = fetchurl {
        name = "pac_proxy_agent___pac_proxy_agent_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/pac-proxy-agent/-/pac-proxy-agent-4.1.0.tgz";
        sha1 = "66883eeabadc915fc5e95457324cb0f0ac78defb";
      };
    }
    {
      name = "pac_resolver___pac_resolver_4.2.0.tgz";
      path = fetchurl {
        name = "pac_resolver___pac_resolver_4.2.0.tgz";
        url  = "https://registry.yarnpkg.com/pac-resolver/-/pac-resolver-4.2.0.tgz";
        sha1 = "b82bcb9992d48166920bc83c7542abb454bd9bdd";
      };
    }
    {
      name = "parseurl___parseurl_1.3.3.tgz";
      path = fetchurl {
        name = "parseurl___parseurl_1.3.3.tgz";
        url  = "https://registry.yarnpkg.com/parseurl/-/parseurl-1.3.3.tgz";
        sha1 = "9da19e7bee8d12dff0513ed5b76957793bc2e8d4";
      };
    }
    {
      name = "path_is_absolute___path_is_absolute_1.0.1.tgz";
      path = fetchurl {
        name = "path_is_absolute___path_is_absolute_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/path-is-absolute/-/path-is-absolute-1.0.1.tgz";
        sha1 = "174b9268735534ffbc7ace6bf53a5a9e1b5c5f5f";
      };
    }
    {
      name = "path_to_regexp___path_to_regexp_0.1.7.tgz";
      path = fetchurl {
        name = "path_to_regexp___path_to_regexp_0.1.7.tgz";
        url  = "https://registry.yarnpkg.com/path-to-regexp/-/path-to-regexp-0.1.7.tgz";
        sha1 = "df604178005f522f15eb4490e7247a1bfaa67f8c";
      };
    }
    {
      name = "pem___pem_1.14.4.tgz";
      path = fetchurl {
        name = "pem___pem_1.14.4.tgz";
        url  = "https://registry.yarnpkg.com/pem/-/pem-1.14.4.tgz";
        sha1 = "a68c70c6e751ccc5b3b5bcd7af78b0aec1177ff9";
      };
    }
    {
      name = "prelude_ls___prelude_ls_1.1.2.tgz";
      path = fetchurl {
        name = "prelude_ls___prelude_ls_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/prelude-ls/-/prelude-ls-1.1.2.tgz";
        sha1 = "21932a549f5e52ffd9a827f570e04be62a97da54";
      };
    }
    {
      name = "proxy_addr___proxy_addr_2.0.6.tgz";
      path = fetchurl {
        name = "proxy_addr___proxy_addr_2.0.6.tgz";
        url  = "https://registry.yarnpkg.com/proxy-addr/-/proxy-addr-2.0.6.tgz";
        sha1 = "fdc2336505447d3f2f2c638ed272caf614bbb2bf";
      };
    }
    {
      name = "proxy_agent___proxy_agent_4.0.1.tgz";
      path = fetchurl {
        name = "proxy_agent___proxy_agent_4.0.1.tgz";
        url  = "https://registry.yarnpkg.com/proxy-agent/-/proxy-agent-4.0.1.tgz";
        sha1 = "326c3250776c7044cd19655ccbfadf2e065a045c";
      };
    }
    {
      name = "proxy_from_env___proxy_from_env_1.1.0.tgz";
      path = fetchurl {
        name = "proxy_from_env___proxy_from_env_1.1.0.tgz";
        url  = "https://registry.yarnpkg.com/proxy-from-env/-/proxy-from-env-1.1.0.tgz";
        sha1 = "e102f16ca355424865755d2c9e8ea4f24d58c3e2";
      };
    }
    {
      name = "pump___pump_3.0.0.tgz";
      path = fetchurl {
        name = "pump___pump_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/pump/-/pump-3.0.0.tgz";
        sha1 = "b4a2116815bde2f4e1ea602354e8c75565107a64";
      };
    }
    {
      name = "qs___qs_6.7.0.tgz";
      path = fetchurl {
        name = "qs___qs_6.7.0.tgz";
        url  = "https://registry.yarnpkg.com/qs/-/qs-6.7.0.tgz";
        sha1 = "41dc1a015e3d581f1621776be31afb2876a9b1bc";
      };
    }
    {
      name = "range_parser___range_parser_1.2.1.tgz";
      path = fetchurl {
        name = "range_parser___range_parser_1.2.1.tgz";
        url  = "https://registry.yarnpkg.com/range-parser/-/range-parser-1.2.1.tgz";
        sha1 = "3cf37023d199e1c24d1a55b84800c2f3e6468031";
      };
    }
    {
      name = "raw_body___raw_body_2.4.0.tgz";
      path = fetchurl {
        name = "raw_body___raw_body_2.4.0.tgz";
        url  = "https://registry.yarnpkg.com/raw-body/-/raw-body-2.4.0.tgz";
        sha1 = "a1ce6fb9c9bc356ca52e89256ab59059e13d0332";
      };
    }
    {
      name = "raw_body___raw_body_2.4.1.tgz";
      path = fetchurl {
        name = "raw_body___raw_body_2.4.1.tgz";
        url  = "https://registry.yarnpkg.com/raw-body/-/raw-body-2.4.1.tgz";
        sha1 = "30ac82f98bb5ae8c152e67149dac8d55153b168c";
      };
    }
    {
      name = "readable_stream___readable_stream_1.1.14.tgz";
      path = fetchurl {
        name = "readable_stream___readable_stream_1.1.14.tgz";
        url  = "https://registry.yarnpkg.com/readable-stream/-/readable-stream-1.1.14.tgz";
        sha1 = "7cf4c54ef648e3813084c636dd2079e166c081d9";
      };
    }
    {
      name = "readable_stream___readable_stream_3.6.0.tgz";
      path = fetchurl {
        name = "readable_stream___readable_stream_3.6.0.tgz";
        url  = "https://registry.yarnpkg.com/readable-stream/-/readable-stream-3.6.0.tgz";
        sha1 = "337bbda3adc0706bd3e024426a286d4b4b2c9198";
      };
    }
    {
      name = "requires_port___requires_port_1.0.0.tgz";
      path = fetchurl {
        name = "requires_port___requires_port_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/requires-port/-/requires-port-1.0.0.tgz";
        sha1 = "925d2601d39ac485e091cf0da5c6e694dc3dcaff";
      };
    }
    {
      name = "rotating_file_stream___rotating_file_stream_2.1.5.tgz";
      path = fetchurl {
        name = "rotating_file_stream___rotating_file_stream_2.1.5.tgz";
        url  = "https://registry.yarnpkg.com/rotating-file-stream/-/rotating-file-stream-2.1.5.tgz";
        sha1 = "6490d0a09e11dd4d441aa5d4d3676debed4a44e4";
      };
    }
    {
      name = "router___router_2.0.0_alpha.1.tgz";
      path = fetchurl {
        name = "router___router_2.0.0_alpha.1.tgz";
        url  = "https://registry.yarnpkg.com/router/-/router-2.0.0-alpha.1.tgz";
        sha1 = "9188213b972215e03ef830e0ac77837870085f6d";
      };
    }
    {
      name = "safe_buffer___safe_buffer_5.1.2.tgz";
      path = fetchurl {
        name = "safe_buffer___safe_buffer_5.1.2.tgz";
        url  = "https://registry.yarnpkg.com/safe-buffer/-/safe-buffer-5.1.2.tgz";
        sha1 = "991ec69d296e0313747d59bdfd2b745c35f8828d";
      };
    }
    {
      name = "safe_buffer___safe_buffer_5.2.1.tgz";
      path = fetchurl {
        name = "safe_buffer___safe_buffer_5.2.1.tgz";
        url  = "https://registry.yarnpkg.com/safe-buffer/-/safe-buffer-5.2.1.tgz";
        sha1 = "1eaf9fa9bdb1fdd4ec75f58f9cdb4e6b7827eec6";
      };
    }
    {
      name = "safe_compare___safe_compare_1.1.4.tgz";
      path = fetchurl {
        name = "safe_compare___safe_compare_1.1.4.tgz";
        url  = "https://registry.yarnpkg.com/safe-compare/-/safe-compare-1.1.4.tgz";
        sha1 = "5e0128538a82820e2e9250cd78e45da6786ba593";
      };
    }
    {
      name = "safer_buffer___safer_buffer_2.1.2.tgz";
      path = fetchurl {
        name = "safer_buffer___safer_buffer_2.1.2.tgz";
        url  = "https://registry.yarnpkg.com/safer-buffer/-/safer-buffer-2.1.2.tgz";
        sha1 = "44fa161b0187b9549dd84bb91802f9bd8385cd6a";
      };
    }
    {
      name = "semver___semver_7.3.5.tgz";
      path = fetchurl {
        name = "semver___semver_7.3.5.tgz";
        url  = "https://registry.yarnpkg.com/semver/-/semver-7.3.5.tgz";
        sha1 = "0b621c879348d8998e4b0e4be94b3f12e6018ef7";
      };
    }
    {
      name = "send___send_0.17.1.tgz";
      path = fetchurl {
        name = "send___send_0.17.1.tgz";
        url  = "https://registry.yarnpkg.com/send/-/send-0.17.1.tgz";
        sha1 = "c1d8b059f7900f7466dd4938bdc44e11ddb376c8";
      };
    }
    {
      name = "serve_static___serve_static_1.14.1.tgz";
      path = fetchurl {
        name = "serve_static___serve_static_1.14.1.tgz";
        url  = "https://registry.yarnpkg.com/serve-static/-/serve-static-1.14.1.tgz";
        sha1 = "666e636dc4f010f7ef29970a88a674320898b2f9";
      };
    }
    {
      name = "setprototypeof___setprototypeof_1.1.0.tgz";
      path = fetchurl {
        name = "setprototypeof___setprototypeof_1.1.0.tgz";
        url  = "https://registry.yarnpkg.com/setprototypeof/-/setprototypeof-1.1.0.tgz";
        sha1 = "d0bd85536887b6fe7c0d818cb962d9d91c54e656";
      };
    }
    {
      name = "setprototypeof___setprototypeof_1.1.1.tgz";
      path = fetchurl {
        name = "setprototypeof___setprototypeof_1.1.1.tgz";
        url  = "https://registry.yarnpkg.com/setprototypeof/-/setprototypeof-1.1.1.tgz";
        sha1 = "7e95acb24aa92f5885e0abef5ba131330d4ae683";
      };
    }
    {
      name = "smart_buffer___smart_buffer_4.1.0.tgz";
      path = fetchurl {
        name = "smart_buffer___smart_buffer_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/smart-buffer/-/smart-buffer-4.1.0.tgz";
        sha1 = "91605c25d91652f4661ea69ccf45f1b331ca21ba";
      };
    }
    {
      name = "socks_proxy_agent___socks_proxy_agent_5.0.0.tgz";
      path = fetchurl {
        name = "socks_proxy_agent___socks_proxy_agent_5.0.0.tgz";
        url  = "https://registry.yarnpkg.com/socks-proxy-agent/-/socks-proxy-agent-5.0.0.tgz";
        sha1 = "7c0f364e7b1cf4a7a437e71253bed72e9004be60";
      };
    }
    {
      name = "socks___socks_2.6.1.tgz";
      path = fetchurl {
        name = "socks___socks_2.6.1.tgz";
        url  = "https://registry.yarnpkg.com/socks/-/socks-2.6.1.tgz";
        sha1 = "989e6534a07cf337deb1b1c94aaa44296520d30e";
      };
    }
    {
      name = "source_map___source_map_0.6.1.tgz";
      path = fetchurl {
        name = "source_map___source_map_0.6.1.tgz";
        url  = "https://registry.yarnpkg.com/source-map/-/source-map-0.6.1.tgz";
        sha1 = "74722af32e9614e9c287a8d0bbde48b5e2f1a263";
      };
    }
    {
      name = "split2___split2_3.2.2.tgz";
      path = fetchurl {
        name = "split2___split2_3.2.2.tgz";
        url  = "https://registry.yarnpkg.com/split2/-/split2-3.2.2.tgz";
        sha1 = "bf2cf2a37d838312c249c89206fd7a17dd12365f";
      };
    }
    {
      name = "statuses___statuses_1.5.0.tgz";
      path = fetchurl {
        name = "statuses___statuses_1.5.0.tgz";
        url  = "https://registry.yarnpkg.com/statuses/-/statuses-1.5.0.tgz";
        sha1 = "161c7dac177659fd9811f43771fa99381478628c";
      };
    }
    {
      name = "string_decoder___string_decoder_1.3.0.tgz";
      path = fetchurl {
        name = "string_decoder___string_decoder_1.3.0.tgz";
        url  = "https://registry.yarnpkg.com/string_decoder/-/string_decoder-1.3.0.tgz";
        sha1 = "42f114594a46cf1a8e30b0a84f56c78c3edac21e";
      };
    }
    {
      name = "string_decoder___string_decoder_0.10.31.tgz";
      path = fetchurl {
        name = "string_decoder___string_decoder_0.10.31.tgz";
        url  = "https://registry.yarnpkg.com/string_decoder/-/string_decoder-0.10.31.tgz";
        sha1 = "62e203bc41766c6c28c9fc84301dab1c5310fa94";
      };
    }
    {
      name = "tar_fs___tar_fs_2.1.1.tgz";
      path = fetchurl {
        name = "tar_fs___tar_fs_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/tar-fs/-/tar-fs-2.1.1.tgz";
        sha1 = "489a15ab85f1f0befabb370b7de4f9eb5cbe8784";
      };
    }
    {
      name = "tar_stream___tar_stream_2.2.0.tgz";
      path = fetchurl {
        name = "tar_stream___tar_stream_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/tar-stream/-/tar-stream-2.2.0.tgz";
        sha1 = "acad84c284136b060dc3faa64474aa9aebd77287";
      };
    }
    {
      name = "toidentifier___toidentifier_1.0.0.tgz";
      path = fetchurl {
        name = "toidentifier___toidentifier_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/toidentifier/-/toidentifier-1.0.0.tgz";
        sha1 = "7e1be3470f1e77948bc43d94a3c8f4d7752ba553";
      };
    }
    {
      name = "tslib___tslib_2.2.0.tgz";
      path = fetchurl {
        name = "tslib___tslib_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/tslib/-/tslib-2.2.0.tgz";
        sha1 = "fb2c475977e35e241311ede2693cee1ec6698f5c";
      };
    }
    {
      name = "type_check___type_check_0.3.2.tgz";
      path = fetchurl {
        name = "type_check___type_check_0.3.2.tgz";
        url  = "https://registry.yarnpkg.com/type-check/-/type-check-0.3.2.tgz";
        sha1 = "5884cab512cf1d355e3fb784f30804b2b520db72";
      };
    }
    {
      name = "type_is___type_is_1.6.18.tgz";
      path = fetchurl {
        name = "type_is___type_is_1.6.18.tgz";
        url  = "https://registry.yarnpkg.com/type-is/-/type-is-1.6.18.tgz";
        sha1 = "4e552cd05df09467dcbc4ef739de89f2cf37c131";
      };
    }
    {
      name = "universalify___universalify_0.1.2.tgz";
      path = fetchurl {
        name = "universalify___universalify_0.1.2.tgz";
        url  = "https://registry.yarnpkg.com/universalify/-/universalify-0.1.2.tgz";
        sha1 = "b646f69be3942dabcecc9d6639c80dc105efaa66";
      };
    }
    {
      name = "unpipe___unpipe_1.0.0.tgz";
      path = fetchurl {
        name = "unpipe___unpipe_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/unpipe/-/unpipe-1.0.0.tgz";
        sha1 = "b2bf4ee8514aae6165b4817829d21b2ef49904ec";
      };
    }
    {
      name = "util_deprecate___util_deprecate_1.0.2.tgz";
      path = fetchurl {
        name = "util_deprecate___util_deprecate_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/util-deprecate/-/util-deprecate-1.0.2.tgz";
        sha1 = "450d4dc9fa70de732762fbd2d4a28981419a0ccf";
      };
    }
    {
      name = "utils_merge___utils_merge_1.0.1.tgz";
      path = fetchurl {
        name = "utils_merge___utils_merge_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/utils-merge/-/utils-merge-1.0.1.tgz";
        sha1 = "9f95710f50a267947b2ccc124741c1028427e713";
      };
    }
    {
      name = "vary___vary_1.1.2.tgz";
      path = fetchurl {
        name = "vary___vary_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/vary/-/vary-1.1.2.tgz";
        sha1 = "2299f02c6ded30d4a5961b0b9f74524a18f634fc";
      };
    }
    {
      name = "which___which_2.0.2.tgz";
      path = fetchurl {
        name = "which___which_2.0.2.tgz";
        url  = "https://registry.yarnpkg.com/which/-/which-2.0.2.tgz";
        sha1 = "7c6a8dd0a636a0327e10b59c9286eee93f3f51b1";
      };
    }
    {
      name = "word_wrap___word_wrap_1.2.3.tgz";
      path = fetchurl {
        name = "word_wrap___word_wrap_1.2.3.tgz";
        url  = "https://registry.yarnpkg.com/word-wrap/-/word-wrap-1.2.3.tgz";
        sha1 = "610636f6b1f703891bd34771ccb17fb93b47079c";
      };
    }
    {
      name = "wrappy___wrappy_1.0.2.tgz";
      path = fetchurl {
        name = "wrappy___wrappy_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/wrappy/-/wrappy-1.0.2.tgz";
        sha1 = "b5243d8f3ec1aa35f1364605bc0d1036e30ab69f";
      };
    }
    {
      name = "ws___ws_7.4.6.tgz";
      path = fetchurl {
        name = "ws___ws_7.4.6.tgz";
        url  = "https://registry.yarnpkg.com/ws/-/ws-7.4.6.tgz";
        sha1 = "5654ca8ecdeee47c33a9a4bf6d28e2be2980377c";
      };
    }
    {
      name = "xdg_basedir___xdg_basedir_4.0.0.tgz";
      path = fetchurl {
        name = "xdg_basedir___xdg_basedir_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/xdg-basedir/-/xdg-basedir-4.0.0.tgz";
        sha1 = "4bc8d9984403696225ef83a1573cbbcb4e79db13";
      };
    }
    {
      name = "xregexp___xregexp_2.0.0.tgz";
      path = fetchurl {
        name = "xregexp___xregexp_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/xregexp/-/xregexp-2.0.0.tgz";
        sha1 = "52a63e56ca0b84a7f3a5f3d61872f126ad7a5943";
      };
    }
    {
      name = "yallist___yallist_3.1.1.tgz";
      path = fetchurl {
        name = "yallist___yallist_3.1.1.tgz";
        url  = "https://registry.yarnpkg.com/yallist/-/yallist-3.1.1.tgz";
        sha1 = "dbb7daf9bfd8bac9ab45ebf602b8cbad0d5d08fd";
      };
    }
    {
      name = "yallist___yallist_4.0.0.tgz";
      path = fetchurl {
        name = "yallist___yallist_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/yallist/-/yallist-4.0.0.tgz";
        sha1 = "9bb92790d9c0effec63be73519e11a35019a3a72";
      };
    }
    {
      name = "yarn___yarn_1.22.10.tgz";
      path = fetchurl {
        name = "yarn___yarn_1.22.10.tgz";
        url  = "https://registry.yarnpkg.com/yarn/-/yarn-1.22.10.tgz";
        sha1 = "c99daa06257c80f8fa2c3f1490724e394c26b18c";
      };
    }
  ];
}
