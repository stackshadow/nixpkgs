# This imports the nix package collection,
# so we can access the `pkgs` and `stdenv` variables
with import <nixpkgs> { };
let

  derivation = import ./derivation.nix { };
  version = "3.10.2";
  # Make a new "derivation" that represents our shell
in stdenv.mkDerivation {
  name = derivation.name;

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    pkgs.gnumake
    pkgs.nodejs
    pkgs.yarn
    pkgs.yarn2nix

    derivation
  ];

  shellHook = ''
    echo "Welcome to code-server-dev-shell"
    echo "You can use 'refresh' to create yarn.nix"
    alias refresh="yarn2nix > yarn.nix"
  '';

  # yarn add code-server --unsafe-perm
}
