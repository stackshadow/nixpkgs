{ system ? builtins.currentSystem, pkgs ? import <nixpkgs> { inherit system; }
}:

rec {
  code-server = pkgs.callPackage ./derivation.nix { };
}
