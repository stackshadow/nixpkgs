# nix-shell -p yarn2nix --command "yarn2nix > yarn.nix"

{ pkgs ? import <nixpkgs> { } }:
let
  stdenv = pkgs.stdenv;

  src = ./.;

  ui = pkgs.mkYarnPackage {
    name = "code-server";
    src = src;
    packageJSON = ./package.json;
    yarnLock = "${src}/yarn.lock";
    yarnNix = ./yarn.nix;

    buildInputs = with pkgs; [ coreutils nodejs yarn ];

    configurePhase = ''
      cp -r $node_modules ./node_modules
      #rsync -r $node_modules/../deps/code-server/node_modules/ ./node_modules
      patchShebangs --build ./node_modules
    '';

    buildPhase = ''
      export PATH=node_modules/.bin:$PATH

      # install code-server
      ${pkgs.yarn}/bin/yarn install --offline
    '';
    installPhase = ''
      # Copy everything
      mkdir -p $out/node_modules
      ${pkgs.rsync}/bin/rsync -avr node_modules/ $out/node_modules/

      mkdir -p $out/bin
      ln -fs $out/node_modules/code-server/out/node/entry.js $out/bin/code-server
    '';
    distPhase = "true";
  };

in ui

