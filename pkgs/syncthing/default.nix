{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { inherit system; };

  callPackage = pkgs.lib.callPackageWith (pkgs // self);
  syncthingPackage = import ./derivation.nix;

  self = {
    v1_16_0 = callPackage syncthingPackage { syncthingVersion = "1.16.0"; };
    v1_16_1 = callPackage syncthingPackage { syncthingVersion = "1.16.1"; };
    latest = self.v1_16_1;
  };

in self
