{ buildGoModule, stdenv, lib, procps, fetchFromGitHub, nixosTests, fetchpatch
, syncthingVersion }:

let
  common = { stname, target, syncthingVersion, postInstall ? "" }:
    buildGoModule rec {
      version = syncthingVersion;
      name = "${stname}-${version}";

      src = fetchFromGitHub {
        owner = "syncthing";
        repo = "syncthing";
        rev = "v${version}";
        sha256 = "1p5wmcmv72hbd3dap9hqv4ryarsj8ljn833x9mcfgh8ff4k25qwr";
      };

      vendorSha256 = "1mwjfv0l2n21srxsh8w18my2j8diim91jlg00ailiq9fwnvxxn8c";

      doCheck = false;

      patches = [
        ./add-stcli-target.patch
        (fetchpatch {
          name = "CVE-2021-21404.patch";
          url =
            "https://github.com/syncthing/syncthing/commit/fb4fdaf4c0a79c22cad000c42ac1394e3ccb6a97.patch";
          sha256 = "0xjh500fi50570dkf3xdaj2367ynn1cs95lypq6b0wi81kp7m540";
        })
      ];
      BUILD_USER = "nix";
      BUILD_HOST = "nix";

      buildPhase = ''
        runHook preBuild
        go run build.go -no-upgrade -version v${version} build ${target}
        runHook postBuild
      '';

      installPhase = ''
        runHook preInstall
        install -Dm755 ${target} $out/bin/${target}
        runHook postInstall
      '';

      inherit postInstall;

      passthru.tests = with nixosTests; {
        init = syncthing-init;
        relay = syncthing-relay;
      };

      meta = with lib; {
        homepage = "https://syncthing.net/";
        description = "Open Source Continuous File Synchronization";
        license = licenses.mpl20;
        maintainers = with maintainers; [ pshendry joko peterhoeg andrew-d ];
        platforms = platforms.unix;
      };
    };

in common {
  stname = "syncthing";
  target = "syncthing";
  inherit syncthingVersion;

  postInstall = ''
    # This installs man pages in the correct directory according to the suffix
    # on the filename
    for mf in man/*.[1-9]; do
      mantype="$(echo "$mf" | awk -F"." '{print $NF}')"
      mandir="$out/share/man/man$mantype"
      install -Dm644 "$mf" "$mandir/$(basename "$mf")"
    done

  '';
}

