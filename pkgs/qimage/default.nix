{ pkgs ? import <nixpkgs> { }, ... }:

let

  qemuImage = import ./qimage.nix {};

  helloWorld = pkgs.writeScriptBin "helloWorld" ''
    #!${pkgs.stdenv.shell}
    echo "${qemuImage}"
  '';

in qemuImage
