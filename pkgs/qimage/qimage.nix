{ pkgs ? import <nixpkgs> { }
, additionalConfig ? builtins.toPath ./. + "/qimage-configuration-extra.nix"
, diskSize ? 2 * 1024 }:

let
  # functions
  makeDiskImage = import <nixpkgs/nixos/lib/make-disk-image.nix>;
  evalConfig = import <nixpkgs/nixos/lib/eval-config.nix>;

  # the config for the target image
  config = (evalConfig {
    modules =
      [ (import ./qimage-configuration-base.nix) (import additionalConfig) ];
  }).config;

  qImage = makeDiskImage {
    inherit pkgs config diskSize;
    lib = pkgs.lib;
    format = "qcow2-compressed";
  };

in qImage
