{ lib, stdenv, nodejs-14_x, writeShellScriptBin }:
let
  system = stdenv.hostPlatform.system;

  src = builtins.fetchTarball {
    url =
      "https://github.com/cdr/code-server/releases/download/v3.12.0/code-server-3.12.0-linux-amd64.tar.gz";
  };

  startCodeServer = writeShellScriptBin "code-server" ''
    ${nodejs-14_x}/bin/node ${src}/out/node/entry.js $@
  '';

in startCodeServer

