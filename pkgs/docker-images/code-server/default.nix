# Use docker load < ./ to load the docker image

{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { inherit system; };

  dockerTools = pkgs.dockerTools;
  callPackage = pkgs.lib.callPackageWith (pkgs // self);

  buildDockerImageLayered = import ../lib/buildImageLayered.nix;
  buildDockerImage = import ../lib/buildImage.nix;
  buildDockerImageFull = import ../lib/buildImageFull.nix;
  code-server = pkgs.callPackage ../../pkgs/code-server/derivation.nix { };


  # dockerImageBase = pkgs.callPackage buildDockerImageLayered {
  #   drv = code-server;
  #   extraContents = with pkgs; [ busybox zsh nodejs yarn nixfmt go_1_16 buildah buildah-unwrapped docker docker-compose ];
  # };

  # dockerImage = pkgs.callPackage buildDockerImage {
  #   imageName = "code-server";
  #   layeredImage = dockerImageBase;
  #   binName = [
  #     "/bin/node"
  #     "/opt/code-server/node_modules/.bin/code-server"
  #     "--disable-telemetry"
  #     "--bind-addr"
  #     "0.0.0.0:4444"
  #     "--auth"
  #     "none"
  #   ];

  # };

  dockerImage = pkgs.callPackage buildDockerImageFull {
    drv = code-server;
    extraContents = with pkgs; [ busybox zsh nodejs yarn nixfmt go_1_16 buildah buildah-unwrapped docker docker-compose ];

    imageName = "code-server";

    binName = [
      "/bin/node"
      "/opt/code-server/node_modules/.bin/code-server"
      "--disable-telemetry"
      "--bind-addr"
      "0.0.0.0:4444"
      "--auth"
      "none"
    ];

  };

  #runAsRoot = ''
  #  #!${pkgs.runtimeShell}
  #  #rm -fR /nix/store/*-gcc-*
  #'';

  #config = {

  #  WorkingDir = "/opt/code-server";
  #  Volumes = { "/data" = { }; };
  #};

  code-server-docker = pkgs.writeShellScriptBin "docker-images-load" ''
    ${pkgs.docker}/bin/docker load < ${dockerImage}
  '';

  self = {
    #execpermfix = callPackage ./execpermfix.nix {};
    #ogle = callPackage ./ogle.nix {};
    #image = callPackage dockerImage {
    #  drv = code-server;
    #  binName =
    #    "/bin/node /opt/code-server/node_modules/.bin/code-server --disable-telemetry --bind-addr 0.0.0.0:4444 --auth none";
    #  extraContents = [ pkgs.nodejs ];
    #};
    image = code-server-docker;
  };
in self
