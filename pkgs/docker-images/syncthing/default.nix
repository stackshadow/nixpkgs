# Use docker load < ./ to load the docker image

{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { inherit system; };

  dockerTools = pkgs.dockerTools;

  # functions
  callPackage = pkgs.lib.callPackageWith
    (pkgs); # this cause nix-build to build this package
  buildDockerImageFull = import ../lib/buildImageFull.nix;

  # packages
  syncthingPackages = import ../../../pkgs/syncthing/default.nix { };

  # docker-image
  dockerImage = callPackage buildDockerImageFull {
    drv = syncthingPackages.latest;
    extraContents = with pkgs; [ busybox ];

    imageName = "stackshadow/nix-syncthing";
    imageTag = "latest-x86_64";
    softwareVersion = "1.16.1";

    binName = [
      "/bin/syncthing"
      "-home"
      "/var/syncthing/config"
      "-gui-address"
      "0.0.0.0:8384"
    ];

  };

  self = {
    latest = dockerImage;
  };
in self
