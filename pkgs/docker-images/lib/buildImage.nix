{ dockerTools, busybox, cacert, imageName # derivation to build the image for
, imageTag ? "latest", layeredImage # needed
, binName ? "/bin/sh" # Name of the binary to run by default
, extraContents ? [ ] }:

dockerTools.buildImage {
  name = imageName;
  tag = imageTag;
  created = "now";

  fromImage = layeredImage;
  #fromImageName = null;
  #fromImageTag = "latest";

  runAsRoot = ''
  '';

  config = {
    Cmd = binName;
    Env = [ "PATH=/bin" "SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt" ];
    Labels = {
      # https://github.com/microscaling/microscaling/blob/55a2d7b91ce7513e07f8b1fd91bbed8df59aed5a/Dockerfile#L22-L33
      "org.label-schema.vcs-ref" = "master";
    };
  };

}
