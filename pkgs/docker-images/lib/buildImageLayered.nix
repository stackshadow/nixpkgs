{ dockerTools, busybox, cacert

# derivation to build the image for
, drv, extraContents ? [ ], meta ? drv.meta, imageName, imageTag ? "latest"
, fromImage ? null, maxLayers ? 50, ... }:
let
  image = dockerTools.buildLayeredImage {
    name = imageName;
    tag = imageTag;
    created = "now";

    fromImage = fromImage;

    maxLayers = maxLayers;

    contents = [
      # most program need TLS certs
      cacert

      # and our derivation
      drv
    ] ++ extraContents;

  };
in image // { meta = meta // image.meta; }
