{ config, dockerImage ? null, compose, stackName }:
let
  pkgs = import <nixpkgs> { };

  scriptLoadDockerFile = if dockerImage != null then
    "${pkgs.docker}/bin/docker load < ${dockerImage}"
  else
    "";

in {

  systemd.services."docker-stack-${stackName}" = {
    description = "Deploy ${stackName} stack";
    wantedBy = [ "multi-user.target" ];
    after = [ ];
    requires = [ ];

    environment = {
      DOCKER_HOST = "${config.environment.variables.DOCKER_HOST}";
    };

    script = ''
      ${scriptLoadDockerFile}
      ${pkgs.docker}/bin/docker stack deploy -c ${compose} ${stackName}
    '';
    serviceConfig = { Type = "oneshot"; };

  };

}
