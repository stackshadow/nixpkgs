{ dockerTools, busybox, cacert, imageName, imageTag ? "latest"
, softwareVersion ? "0.0.0", layeredImage ? null, baseImage ? null
, baseImageTag ? "", drv
, binName ? "/bin/sh" # Name of the binary to run by default
, extraContents ? [ ], runAsRoot ? "", diskSize ? 1024 }:

dockerTools.buildImage {
  name = imageName;
  tag = imageTag;
  created = "now";

  fromImage = layeredImage;
  fromImageName = baseImage;
  fromImageTag = baseImageTag;

  diskSize = diskSize;

  contents = [
    # most program need TLS certs
    cacert

    # and our derivation
    drv
  ] ++ extraContents;

  runAsRoot = runAsRoot;

  config = {
    Cmd = binName;
    Env = [ "PATH=/bin" "SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt" ];
    Labels = {
      # https://github.com/microscaling/microscaling/blob/55a2d7b91ce7513e07f8b1fd91bbed8df59aed5a/Dockerfile#L22-L33
      "org.label-schema.vcs-ref" = "master";
      "version" = "${softwareVersion}";
    };
  };

}
