{ system ? builtins.currentSystem, admindn ? "cn=admin,cn=config"
, adminpw ? "secret", host ? "ldap://127.0.0.1:9000" }:

let

  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;

  ldiffMemberof = pkgs.writeTextFile {
    name = "memberof.ldif";
    executable = false;
    destination = "/etc/schema/memberof.ldif";
    text = builtins.readFile ./memberof.ldif;
  };

  ldiffOLC = pkgs.writeTextFile {
    name = "olc.ldif";
    executable = false;
    destination = "/etc/schema/olc.ldif";
    text = builtins.readFile ./olc.ldif;
  };

  ldiffRefint = pkgs.writeTextFile {
    name = "refint.ldif";
    executable = false;
    destination = "/etc/schema/refint.ldif";
    text = builtins.readFile ./refint.ldif;
  };

  ldiffSSHA = pkgs.writeTextFile {
    name = "ssha.ldif";
    executable = false;
    destination = "/etc/schema/ssha.ldif";
    text = builtins.readFile ./ssha.ldif;
  };

  additionalSchemas = pkgs.symlinkJoin {
    name = "openldap-additional-schemas";
    paths = [ ldiffMemberof ldiffOLC ldiffRefint ldiffSSHA ];
  };

in additionalSchemas
