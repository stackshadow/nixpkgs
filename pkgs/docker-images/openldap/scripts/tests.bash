#!/bin/bash
set -e

ldapsearch \
-p 9000 \
-h distroless \
-D ${ADMINDN} -w ${ADMINPW} \
-b cn=config | grep "olcSuffix: ${DBSUFFIX}"

ldapsearch \
-p 9000 \
-h distroless \
-D ${DBADMIN} -w ${DBPW} \
-b ${DBSUFFIX} | grep "search result"
