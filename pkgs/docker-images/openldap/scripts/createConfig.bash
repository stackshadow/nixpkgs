#!/bin/bash


if [ ! -z "${ADMINDN}" ]; then
if [ ! -z "${ADMINPW}" ]; then
echo "################################### Create config... ###################################"


cat >/etc/openldap/slapd.conf <<EOF
# basic settings
pidfile         /run/openldap/slapd.pid
argsfile        /run/openldap/slapd.args

# we use OLC
database        config
rootdn          "${ADMINDN}"
rootpw          ${ADMINPW}
EOF

echo "################################### Create config Done ###################################"

fi
fi

