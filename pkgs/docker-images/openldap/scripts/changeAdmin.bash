#!/bin/bash

# we start ldap
/usr/bin/slapd -h ldap://0.0.0.0:9000 -F /etc/openldap/slapd.d

read -p "Enter OLD Admin-DN (default admin is cn=admin,cn=config ) : " oldadmindn
read -s -p "Enter OLD Admin-Password (default is secret): " oldadminpw
echo " "
read -p "Enter Admin-DN: " admindn
read -s -p "Enter Admin-Password: " adminpw
echo " "

cat >/var/lib/openldap/createdb.ldiff <<EOF
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: ${admindn}
-
replace: olcRootPW
olcRootPW: ${adminpw}
EOF

echo "Change cn=admin,cn=config"
ldapmodify -D ${oldadmindn} -w ${oldadminpw} -H ldap://0.0.0.0:9000 -f /var/lib/openldap/createdb.ldiff

exit