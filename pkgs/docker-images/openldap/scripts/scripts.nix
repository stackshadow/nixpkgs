{ system ? builtins.currentSystem, admindn ? "cn=admin,cn=config"
, adminpw ? "secret", host ? "ldap://127.0.0.1:9000" }:

let

  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;

  entrypoint = pkgs.writeTextFile {
    name = "entrypoint.bash";
    executable = true;
    destination = "/bin/entrypoint";
    text = builtins.readFile ./entrypoint.bash;
  };

  prepare = pkgs.writeTextFile {
    name = "prepare.bash";
    executable = true;
    destination = "/bin/prepare";
    text = builtins.readFile ./prepare.bash;
  };

  changeAdmin = pkgs.writeTextFile {
    name = "changeAdmin.bash";
    executable = true;
    destination = "/bin/changeAdmin";
    text = builtins.readFile ./changeAdmin.bash;
  };

  changeDB = pkgs.writeTextFile {
    name = "changeDB.bash";
    executable = true;
    destination = "/bin/changeDB";
    text = builtins.readFile ./changeDB.bash;
  };

  createConfig = pkgs.writeTextFile {
    name = "createConfig.bash";
    executable = true;
    destination = "/bin/createConfig";
    text = builtins.readFile ./createConfig.bash;
  };

  createDB = pkgs.writeTextFile {
    name = "createDB.bash";
    executable = true;
    destination = "/bin/createDB";
    text = builtins.readFile ./createDB.bash;
  };

  createRun = pkgs.writeTextFile {
    name = "createRun.bash";
    executable = true;
    destination = "/bin/createRun";
    text = builtins.readFile ./createRun.bash;
  };

  allscripts = pkgs.symlinkJoin {
    name = "supervisor-nextcloud-configs";
    paths = [ entrypoint prepare changeAdmin changeDB createConfig createDB createRun ];
  };

in allscripts
