#!/bin/bash


# we start ldap
/usr/bin/slapd -h ldap://0.0.0.0:9000 -F /etc/openldap/slapd.d

read -p "Enter Admin dn: " admindn
read -s -p "Enter Admin password: " adminpw
echo " "

read -p "Enter DB-Suffix (example: dc=mnet,dc=local): " dbsuffix


read -p "Enter OLD DB-Admin: " olddbadmin
read -s -p "Enter OLD DB-Password" olddbpw
echo " "


read -p "Enter DB-Admin: " dbadmin
read -s -p "Enter DB-Password" dbpw
echo " "


# get
tempdn=$(slapcat -b cn=config -a olcSuffix=${dbsuffix} | grep dn:)

cat >/var/lib/openldap/createdb.ldiff <<EOF
${tempdn}
changetype: modify
replace: olcSuffix
olcSuffix: ${dbsuffix}
-
replace: olcDbDirectory
olcDbDirectory: /var/lib/openldap/${dbsuffix}
-
replace: olcRootDN
olcRootDN: ${dbadmin}
-
replace: olcRootPW
olcRootPW: ${dbpw}
EOF


ldapmodify -D ${olddbadmin} -w ${olddbpw} -H ldap://0.0.0.0:9000 -f /var/lib/openldap/createdb.ldiff
exit