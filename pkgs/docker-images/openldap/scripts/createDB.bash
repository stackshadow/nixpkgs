#!/bin/bash
set -e

#jobs
#trap "exit" INT TERM ERR
#trap "kill %1" EXIT

LDIFPATH="/tmp/openldap"

if [ -z "${ADMINDN}" ]; then
    read -p "Enter Admin dn: " admindn
    export ADMINDN=${admindn}
fi

if [ -z "${ADMINPW}" ]; then
    read -s -p "Enter Admin password: " adminpw
    export ADMINPW=${adminpw}
fi
echo " "

if [ -z "${DBSUFFIX}" ]; then
    read -p "Enter DB-Suffix (example: dc=mnet,dc=local): " dbsuffix
    export DBSUFFIX=${dbsuffix}
fi
if [ -z "${DBADMIN}" ]; then
    read -p "Enter DB-Admin: (example: cn=admin,${DBSUFFIX}): " dbadmin
    export DBADMIN=${dbadmin}
fi
if [ -z "${DBPW}" ]; then
    read -s -p "Enter DB-Password: " dbpw
    export DBPW=${dbpw}
fi

if [ -z "${ORGA}" ]; then
    read -p "Enter ORGA: (example: test): " dborga
    export ORGA=${dborga}
fi



echo " "

# create directory
mkdir -p /var/openldap/${DBSUFFIX}
mkdir -p $LDIFPATH


echo -e "\n############################ core.ldif ############################\n"
ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/core.ldif && \
sleep 1

echo -e "\n############################ cosine.ldif ############################\n"
ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/cosine.ldif && \
sleep 1



echo "################################### Create mdb,cn=config ###################################"
exist=$(ldapsearch -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -b cn=config "(olcSuffix=${DBSUFFIX})" | grep olcDatabase: || true )

if [ -z "$exist" ]; then
cat >$LDIFPATH/createdb.ldiff <<EOF
# Database settings
dn: olcDatabase=mdb,cn=config
objectClass: olcDatabaseConfig
objectClass: olcMdbConfig
olcDatabase: mdb
olcSuffix: ${DBSUFFIX}
olcDbDirectory: /var/openldap/${DBSUFFIX}
olcRootDN: ${DBADMIN}
olcRootPW: ${DBPW}
olcDbIndex: objectClass eq
olcLastMod: TRUE
olcAccess: to attrs=userPassword by dn="${DBADMIN}" write by anonymous auth by self write by * none
olcAccess: to dn.base="" by * read
olcAccess: to * by dn="${DBADMIN}" write by * read
EOF
    ldapadd -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -f $LDIFPATH/createdb.ldiff
    sleep 2
else
    echo "olcDatabase=mdb,cn=config already exist"
fi

# get mdb-number
IN=$(ldapsearch -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -b cn=config "(olcSuffix=${DBSUFFIX})" | grep olcDatabase:)
MDB=${IN##* }

# This exports the current DB-Config and support to load
ldapsearch -LLL -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -b cn=config "(olcSuffix=${DBSUFFIX})" > /var/openldap/${DBSUFFIX}.ldif

echo "################################### Create memberOf ###################################"
exist=$(ldapsearch -LLL -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -b cn=config | grep olcOverlay=memberof,olcDatabase=${MDB},cn=config || true )

if [ -z "$exist" ]; then
cat >$LDIFPATH/memberof.ldiff <<EOF
dn: olcOverlay=memberof,olcDatabase=${MDB},cn=config
objectClass: olcConfig
objectClass: olcMemberOf
objectClass: olcOverlayConfig
objectClass: top
olcOverlay: memberof
olcMemberOfDangling: ignore
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfNames
olcMemberOfMemberAD: member
olcMemberOfMemberOfAD: memberOf
EOF
    ldapadd -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -f $LDIFPATH/memberof.ldiff
    sleep 2
else
    echo "olcOverlay=memberof,olcDatabase=${MDB},cn=config already exist"
fi

echo "################################### Create refint ###################################"
exist=$(ldapsearch -LLL -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -b cn=config | grep olcOverlay=refint,olcDatabase=${MDB},cn=config || true )

if [ -z "$exist" ]; then
cat >$LDIFPATH/refint.ldiff <<EOF
dn: olcOverlay=refint,olcDatabase=${MDB},cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcRefintConfig
objectClass: top
olcOverlay: refint
olcRefintAttribute: memberof member manager owner
EOF
    ldapadd -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -f $LDIFPATH/refint.ldiff
    sleep 2
else
    echo "olcOverlay=refint,olcDatabase=${MDB},cn=config already exist"
fi


echo "################################### Create ORGA ###################################"
exist=$(ldapsearch -LLL -p 9000 -h 127.0.0.1 -D ${DBADMIN} -w ${DBPW} -b cn=config "(o=${ORGA})" || true )

cat >$LDIFPATH/orga.ldiff <<EOF
dn: ${DBSUFFIX}
dc: ${ORGA}
o: ${ORGA}
objectclass: organization
objectclass: dcObject
EOF
ldapadd -p 9000 -h 127.0.0.1 -D ${DBADMIN} -w ${DBPW} -f $LDIFPATH/orga.ldiff


#ldapsearch -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -b cn=config
#sleep 2

#ldapsearch -p 9000 -h 127.0.0.1 -D ${DBADMIN} -w ${DBPW} -b ${DBSUFFIX}
#sleep 2

exit