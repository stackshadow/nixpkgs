#!/bin/bash

CMDSUFFIX=""
if [ ! -z "${DEBUG}" ]; then
    CMDSUFFIX="-d 8"
fi

cat >/etc/sv/ldap/run <<EOF
#!/bin/bash
# https://www.openldap.org/doc/admin24/runningslapd.html
exec /usr/bin/slapd -h ldap://0.0.0.0:9000 -F /etc/openldap/slapd.d ${CMDSUFFIX}
EOF
