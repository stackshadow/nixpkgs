
if [ ! -f /var/openldap/.prepared ]; then

    /libexec/slapd -h ldap://0.0.0.0:9000 -f /etc/openldap/slapd.conf -F /var/openldap -d 0 &
    ldappid=$!
    sleep 5
    echo "PID= $ldappid"

    echo -e "\n############################ core.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/core.ldif && \
    sleep 1

    echo -e "\n############################ cosine.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/cosine.ldif && \
    sleep 1

    echo -e "\n############################ nis.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/nis.ldif && \
    sleep 1

    echo -e "\n############################ inetorgperson.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/inetorgperson.ldif && \
    sleep 1

    echo -e "\n############################ openldap.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/openldap.ldif && \
    sleep 1

    echo -e "\n############################ memberof.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/memberof.ldif && \
    sleep 1

    echo -e "\n############################ olc.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/olc.ldif && \
    sleep 1

    echo -e "\n############################ refint.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/refint.ldif && \
    sleep 1

    echo -e "\n############################ ssha.ldif ############################\n"
    ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/ssha.ldif && \
    sleep 1


    touch /var/openldap/.prepared

    echo "Kill $ldappid"
    kill -s KILL $ldappid
    sleep 3
else
    echo "Already prepared"
fi

