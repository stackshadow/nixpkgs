#!/bin/bash

# prepare
/bin/bash /bin/prepare

echo "Start openldap"
/libexec/slapd -h ldap://0.0.0.0:9000 -f /etc/openldap/slapd.conf -F /var/openldap -d 1 &
ldappid=$!
sleep 5


echo -e "\n############################ core.ldif ############################\n"
ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/core.ldif && \
sleep 1

echo -e "\n############################ olc.ldif ############################\n"
ldapadd -D ${ADMINDN} -w ${ADMINPW} -H ldap://127.0.0.1:9000 -f /etc/schema/olc.ldif && \
sleep 1

echo -e "\n############################ /var/openldap/${DBSUFFIX}.ldif ############################\n"
ldapadd -p 9000 -h 127.0.0.1 -D ${ADMINDN} -w ${ADMINPW} -f /var/openldap/${DBSUFFIX}.ldif

wait $ldappid