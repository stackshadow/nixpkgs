# Use docker load < ./ to load the docker image

{ system ? builtins.currentSystem, home ? "/opt/nextcloud" }:

let

  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;

  dockerTools = pkgs.dockerTools;

  # functions
  callPackage = pkgs.lib.callPackageWith
    (pkgs); # this cause nix-build to build this package
  buildDockerImageFull = import ../lib/buildImageFull.nix;

  slapdConfig = pkgs.writeTextFile {
    name = "slapd.conf";
    executable = false;
    destination = "/etc/openldap/slapd.conf";
    text = ''
      # basic settings
      pidfile         /run/slapd.pid
      argsfile        /run/slapd.args

      # we use OLC
      database        config
      rootdn          "cn=admin,cn=config"
      rootpw          secret
    '';
  };

  slapdPrepare = import ./ldiffs/schemas.nix {   };

  slapdScripts = import ./scripts/scripts.nix { };

  # docker-image
  dockerImage = callPackage buildDockerImageFull {
    drv = pkgs.openldap;
    extraContents = with pkgs; [
      slapdConfig
      slapdPrepare
      slapdScripts

      bash
      coreutils
      gnugrep
    ];

    imageName = "stackshadow/nix-openldap";
    imageTag = "latest-x86_64";
    softwareVersion = "2.4.58";

    runAsRoot = ''
      echo "openldap:x:1000:1000::/tmp:${pkgs.bash}/bin/bash" >> /etc/passwd

      mkdir -vp /tmp /run /var/openldap
      chown :1000 -R /tmp /run /var/openldap
      chmod -R g+rwX /tmp /run /var/openldap
    '';

    binName = [ "/bin/bash" "/bin/entrypoint" ];

  };

  # docker-image
  dockerImageDebug = callPackage buildDockerImageFull {
    diskSize = 8000;
    layeredImage = dockerImage;

    drv = pkgs.zsh;
    extraContents = with pkgs; [
      findutils
      ncdu
      dig
      zsh
      nano
      inetutils
      unixtools.netstat
    ];

    imageName = "stackshadow/nix-openldap";
    imageTag = "latest-debug-x86_64";
    softwareVersion = "2.4.58";

    runAsRoot = "";

    binName = [ "/bin/bash" "/bin/entrypoint" ];
  };

  dockerImagesLoad = pkgs.writeShellScriptBin "docker-openldap-load" ''
    ${pkgs.docker}/bin/docker load < ${dockerImage}
    ${pkgs.docker}/bin/docker load < ${dockerImageDebug}
  '';

  self = {
    latest = dockerImage;
    debug = dockerImageDebug;
    load = dockerImagesLoad;
  };
in self
