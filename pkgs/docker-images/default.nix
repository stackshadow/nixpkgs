{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { inherit system; };

  callPackage = pkgs.lib.callPackageWith (pkgs // self);

  self = {
    lib = { docker-stack = import ./lib/docker-stack.nix; };
    syncthing = (callPackage ./syncthing/default.nix { });
  };
in self
