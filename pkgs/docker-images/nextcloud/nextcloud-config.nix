{ pkgs, config, lib, ... }:
let
  inherit (lib) mkDefault mkEnableOption mkForce mkIf mkMerge mkOption types;
  inherit (lib)
    mapAttrs mapAttrs' mapAttrsToList nameValuePair optional optionalAttrs
    optionalString concatMapStringsSep;

  c = config.services.nextcloud;
  writePhpArrary = a:
    "[${concatMapStringsSep "," (val: ''"${toString val}"'') a}]";

  nextCloudConfig = pkgs.writeTextFile {
    name = "nextcloud-conf";
    executable = false;
    destination = "/config/config.php";
    text = ''
      <?php
      ${optionalString (c.dbpassFile != null) ''
        function nix_read_pwd() {
          $file = "${c.dbpassFile}";
          if (!file_exists($file)) {
            throw new \RuntimeException(sprintf(
              "Cannot start Nextcloud, dbpass file %s set by NixOS doesn't seem to "
              . "exist! Please make sure that the file exists and has appropriate "
              . "permissions for user & group 'nextcloud'!",
              $file
            ));
          }
          return trim(file_get_contents($file));
        }
      ''}
      $CONFIG = [
        'apps_paths' => [
          [ 'path' => '${c.home}/nextcloud/apps', 'url' => '/apps', 'writable' => false ],
          [ 'path' => '${c.home}/nextcloud/store-apps', 'url' => '/store-apps', 'writable' => true ],
        ],

        ${
          optionalString (c.instanceid != null) ''
            'instanceid' => '${c.instanceid}',
            'passwordsalt' => '${c.passwordsalt}',
            'secret' => '${c.secret}',
            'version' => '21.0.2.1',
            'overwrite.cli.url' => '${c.overwriteHost}',
            'installed' => true,
            'config_is_read_only' => true,
          ''
        }

        'datadirectory' => '${c.home}/nextcloud/data',
        'skeletondirectory' => '${c.skeletonDirectory}',
        ${
          optionalString c.caching.apcu
          "'memcache.local' => '\\OC\\Memcache\\APCu',"
        }
        'log_type' => 'nextcloud.log',
        'log_level' => '${builtins.toString c.logLevel}',
        ${
          optionalString (c.overwriteProtocol != null)
          "'overwriteprotocol' => '${c.overwriteProtocol}',"
        }
        ${
          optionalString (c.overwriteHost != null)
          "'overwritehost' => '${c.overwriteHost}',"
        }
        ${optionalString (c.dbname != null) "'dbname' => '${c.dbname}',"}
        ${optionalString (c.dbhost != null) "'dbhost' => '${c.dbhost}',"}
        ${
          optionalString (c.dbport != null)
          "'dbport' => '${toString c.dbport}',"
        }
        ${optionalString (c.dbuser != null) "'dbuser' => '${c.dbuser}',"}
        ${
          optionalString (c.dbtableprefix != null)
          "'dbtableprefix' => '${toString c.dbtableprefix}',"
        }
        ${optionalString (c.dbpass != null) "'dbpassword' => '${c.dbpass}',"}
        ${
          optionalString (c.dbpassFile != null)
          "'dbpassword' => nix_read_pwd(),"
        }
        'dbtype' => '${c.dbtype}',
        'trusted_domains' => ${
          writePhpArrary ([ c.hostName ] ++ c.extraTrustedDomains)
        },
        'trusted_proxies' => ${writePhpArrary (c.trustedProxies)},
        ${
          optionalString (c.defaultPhoneRegion != null)
          "'default_phone_region' => '${c.defaultPhoneRegion}',"
        }

      ];
    '';
  };
in nextCloudConfig
