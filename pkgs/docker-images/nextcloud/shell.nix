{ system ? builtins.currentSystem }:

let

  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;
  stdenv = pkgs.stdenv;

  package = import ./default.nix { home = "/tmp/nextcloud"; };

in stdenv.mkDerivation {
  name = "my-environment";

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    pkgs.php
    pkgs.postgresql
    package.entrypoint
    package.occ
    package.start
    package.status
    package.stop
  ];

  shellHook = ''

    echo "${package.entrypoint}"


  '';

  # yarn add code-server --unsafe-perm
}

# # This imports the nix package collection,
# # so we can access the `pkgs` and `stdenv` variables
# with import <nixpkgs> { };
# let
#   inherit (lib)
#     mapAttrs mapAttrs' mapAttrsToList nameValuePair optional optionalAttrs
#     optionalString concatMapStringsSep;

#   version = "3.10.2";
#   # Make a new "derivation" that represents our shell

#   # create a config
#   c = nextcloudNixConfig.services.nextcloud;
#   nextcloudNixConfig = {

#     services.nextcloud = {
#       package = pkgs.nextcloud21;
#       phpEnv = null;

#       home = "/tmp/nextcloud";

#       # if instanceid != null, the config.php will be overwritten !
#       instanceid = null;
#       passwordsalt = "";
#       secret = "";

#       skeletonDirectory = "${c.package}/core/skeleton";
#       caching.apcu = true;

#       dbpassFile = null;
#       logLevel = "WARN";
#       overwriteProtocol = "http";
#       overwriteHost = "127.0.0.1:8686";

#       dbtype = "sqlite";
#       dbname = "";
#       dbhost = "";
#       dbport = "";
#       dbuser = "";
#       dbpass = "";

#       dbtableprefix = null;
#       hostName = "localhost";
#       extraTrustedDomains = [ ];
#       trustedProxies = [ ];
#       defaultPhoneRegion = "DE";

#     };
#   };

#   entrypoint = import ./serviceSupervisord.nix {
#     inherit pkgs;
#     inherit lib;
#     config = nextcloudNixConfig;
#   };

# in stdenv.mkDerivation {
#   name = "my-environment";

#   # The packages in the `buildInputs` list will be added to the PATH in our shell
#   buildInputs = [ php postgresql entrypoint ];

#   shellHook = ''
#     mkdir -p ${c.home}/log ${c.home}/run
#     echo " "

#     . ${entrypoint}/bin/entrypoint
#   '';

#   # yarn add code-server --unsafe-perm
# }
