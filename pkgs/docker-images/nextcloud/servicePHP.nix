{ pkgs, config, ... }:
let
  c = config.services.nextcloud;


  defaultPHPEnv = pkgs.php.buildEnv {
    extensions = { all, ... }:
      with all; [
        ctype
        curl
        dom
        filter
        gd
        iconv
        json
        mbstring
        openssl
        posix
        session
        simplexml
        xmlreader
        xmlwriter
        zip
        zlib
        memcached
        pdo_sqlite
        pdo_pgsql
        fileinfo
        bz2
        intl
        apcu
        opcache
        imagick
        pcntl
        bcmath
        gmp
        ldap
        smbclient
        ftp
        imap
        exif
      ];
    extraConfig = ''
      memory_limit=512M
      opcache.enable=1
      opcache.interned_strings_buffer=8
      opcache.max_accelerated_files=10000
      opcache.memory_consumption=128
      opcache.save_comments=1
      opcache.revalidate_freq=1
    '';
  };


  phpPackage = if c.phpEnv == null then defaultPHPEnv else c.phpEnv;

  phpConfig = pkgs.writeTextFile {
    name = "nextcloud-php-config";
    executable = false;
    destination = "/etc/php-fpm/php-fpm.conf";
    text = ''
      ;;;;;;;;;;;;;;;;;;
      ; Global Options ;
      ;;;;;;;;;;;;;;;;;;
      [global]
      ; Pid file
      ; Note: the default prefix is /var
      ; Default Value: none
      pid = ${c.home}/run/php5-fpm.pid

      ; Error log file
      ; Note: the default prefix is /var
      ; Default Value: log/php-fpm.log
      error_log = ${c.home}/log/php5-fpm.log

      ; Log level
      ; Possible Values: alert, error, warning, notice, debug
      ; Default Value: notice
      ;log_level = notice

      ; If this number of child processes exit with SIGSEGV or SIGBUS within the time
      ; interval set by emergency_restart_interval then FPM will restart. A value
      ; of '0' means 'Off'.
      ; Default Value: 0
      emergency_restart_threshold = 10

      ; Interval of time used by emergency_restart_interval to determine when 
      ; a graceful restart will be initiated.  This can be useful to work around
      ; accidental corruptions in an accelerator's shared memory.
      ; Available Units: s(econds), m(inutes), h(ours), or d(ays)
      ; Default Unit: seconds
      ; Default Value: 0
      emergency_restart_interval = 1m

      ; Time limit for child processes to wait for a reaction on signals from master.
      ; Available units: s(econds), m(inutes), h(ours), or d(ays)
      ; Default Unit: seconds
      ; Default Value: 0
      process_control_timeout = 5

      ; Send FPM to background. Set to 'no' to keep FPM in foreground for debugging.
      ; Default Value: yes
      daemonize = no

      ;;;;;;;;;;;;;;;;;;;;
      ; Pool Definitions ; 
      ;;;;;;;;;;;;;;;;;;;;

      ; Multiple pools of child processes may be started with different listening
      ; ports and different management options.  The name of the pool will be
      ; used in logs and stats. There is no limitation on the number of pools which
      ; FPM can handle. Your system will tell you anyway :)

      ; To configure the pools it is recommended to have one .conf file per
      ; pool in the following directory:
      include=${phpPools}/etc/php5/fpm/pool.d/*.conf
    '';
  };
  /* phpIni =  pkgs.writeTextFile {
       name = "nextcloud-php-ini";
       executable = false;
       destination = "/etc/php/php.ini";
       text = ''
         memory_limit = 512M

         opcache.enable=1
         opcache.interned_strings_buffer=8
         opcache.max_accelerated_files=10000
         opcache.memory_consumption=128
         opcache.save_comments=1
         opcache.revalidate_freq=1

         extension= /lib/php/extensions/apcu.so
         apc.enabled=1

         extension= /lib/php/extensions/imagick.so

         extension=pdo_pgsql.so
         extension=pgsql.so

         [PostgresSQL]
         pgsql.allow_persistent = On
         pgsql.auto_reset_persistent = Off
         pgsql.max_persistent = -1
         pgsql.max_links = -1
         pgsql.ignore_notice = 0
         pgsql.log_notice = 0
       '';
     };
  */
  phpPoolNextCloud = pkgs.writeTextFile {
    name = "nextcloud-php-pool";
    executable = false;
    destination = "/etc/php5/fpm/pool.d/php-fpm-nextcloud.conf";
    text = ''
      [www]
      env[NEXTCLOUD_CONFIG_DIR] = ${c.home}/nextcloud/config
      env[HOSTNAME] = $HOSTNAME
      env[TMP] = /tmp
      env[TMPDIR] = /tmp
      env[TEMP] = /tmp
      user = code-server

      clear_env = no

      listen = 127.0.0.1:9000
      listen.allowed_clients = 127.0.0.1

      pm = dynamic
      pm.max_children = 120
      pm.start_servers = 5
      pm.min_spare_servers = 5
      pm.max_spare_servers = 10

      catch_workers_output = true
    '';
  };

  phpPools = pkgs.symlinkJoin {
    name = "nextcloud-php-pools";
    paths = [ phpPoolNextCloud ];
  };

  cronScript = pkgs.writeTextFile {
    name = "nextcloud-cron";
    executable = true;
    destination = "/bin/nextcloud-cron";
    text = ''
      #!${pkgs.bash}/bin/bash
      export NEXTCLOUD_CONFIG_DIR="${c.home}/nextcloud/config"
      while :
      do
        ${phpPackage}/bin/php ${c.home}/nextcloud/cron.php  
        sleep 300
      done
    '';
  };

  supervisorconf = pkgs.writeTextFile {
    name = "nextcloud-supervisor-php";
    executable = false;
    destination = "/etc/supervisor/conf.d/php.conf";
    text = ''
      [program:php-fpm]
      stdout_logfile=${c.home}/log/supervisor_php-fpm.log
      redirect_stderr=true
      command=${phpPackage}/bin/php-fpm -y ${phpConfig}/etc/php-fpm/php-fpm.conf  
      autostart=true
      autorestart=true
      priority=5
      stdout_events_enabled=true
      stderr_events_enabled=true

      [program:php-cron]
      stdout_logfile=${c.home}/log/supervisor_php-cron.log
      redirect_stderr=true
      command=${pkgs.bash}/bin/bash ${cronScript}/bin/nextcloud-cron
      autostart=true
      autorestart=true
      priority=6
      stdout_events_enabled=true
      stderr_events_enabled=true
    '';
  };

in supervisorconf
