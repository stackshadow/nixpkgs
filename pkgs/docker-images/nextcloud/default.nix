# Use docker load < ./ to load the docker image

{ system ? builtins.currentSystem, home ? "/opt/nextcloud" }:

let

  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;

  dockerTools = pkgs.dockerTools;

  # functions
  callPackage = pkgs.lib.callPackageWith
    (pkgs); # this cause nix-build to build this package
  buildDockerImageFull = import ../lib/buildImageFull.nix;

  # create a config
  c = nextcloudNixConfig.services.nextcloud;
  nextcloudNixConfig = {

    services.nextcloud = {
      package = pkgs.nextcloud21;
      phpEnv = null;

      home = home;

      # if instanceid != null, the config.php will be overwritten !
      instanceid = null;
      passwordsalt = "";
      secret = "";

      skeletonDirectory = "${c.package}/core/skeleton";
      caching.apcu = true;

      dbpassFile = null;
      logLevel = "WARN";
      overwriteProtocol = "http";
      overwriteHost = "127.0.0.1:8686";

      dbtype = "sqlite";
      dbname = "";
      dbhost = "";
      dbport = 0;
      dbuser = "";
      dbpass = "";

      dbtableprefix = null;
      hostName = "localhost";
      extraTrustedDomains = [ ];
      trustedProxies = [ ];
      defaultPhoneRegion = "DE";

    };
  };

  supervisor = import ./serviceSupervisord.nix {
    inherit pkgs;
    inherit lib;
    config = nextcloudNixConfig;
  };

  # docker-image
  dockerImage = callPackage buildDockerImageFull {
    drv = supervisor.entrypoint;
    extraContents = with pkgs; [
      bash
      coreutils
      gnugrep
      supervisor.status
      supervisor.occ
      /*
      ncdu
      dig
      zsh
      nano
      */
    ];

    imageName = "stackshadow/nix-nextcloud";
    imageTag = "latest-x86_64";
    softwareVersion = "21";

    runAsRoot = ''
      mkdir -p ${c.home}
      mkdir -p /tmp
      chmod a+rwX -R ${c.home}
      chmod a+rwX -R /tmp

      echo "code-server:x:1000:1000::${c.home}:${pkgs.bash}/bin/bash" >> /etc/passwd
    '';

    binName = [ "/bin/bash" "-c" ". ${supervisor.entrypoint}/bin/entrypoint" ];

  };

  self = {
    latest = dockerImage;
    occ = supervisor.occ;
    start = supervisor.start;
    status = supervisor.status;
    stop = supervisor.stop;
    entrypoint = supervisor.entrypoint;
  };
in self
