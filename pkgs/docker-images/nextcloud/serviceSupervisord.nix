{ pkgs, lib, config, ... }:
let
  c = config.services.nextcloud;

  supervisor-go = import ../../supervisord-go/default.nix {};
  supervisord = supervisor-go.latest;
  #supervisord = pkgs.python3.pkgs.supervisor;

  serviceNextcloud = import ./serviceNextcloud.nix {
    inherit pkgs;
    inherit lib;
    inherit config;
  };

  servicePHP = import ./servicePHP.nix {
    inherit pkgs;
    inherit config;
  };

  serviceNginx = import ./serviceNginx.nix {
    inherit pkgs;
    inherit config;
  };

  /* servicePostgresql = import ./servicePostgresql.nix {
       inherit pkgs;
       inherit lib;
       inherit config;
     };
  */

  supervisorProgramms = pkgs.symlinkJoin {
    name = "supervisor-nextcloud-configs";
    paths = [
      serviceNextcloud
      servicePHP
      serviceNginx
      #servicePostgresql
    ];
  };

  supervisorConf = pkgs.writeTextFile {
    name = "supervisor-nextcloud-conf";
    executable = false;
    destination = "/etc/supervisor/supervisor.conf";
    text = ''
      [inet_http_server]
      port=127.0.0.1:9001
      ;username=test1
      ;password=thepassword

      [unix_http_server]
      port=127.0.0.1:9001
      ;username=test1
      ;password=thepassword

      ; the below section must remain in the config file for RPC
      ; (supervisorctl/web interface) to work, additional interfaces may be
      ; added by defining them in separate rpcinterface: sections
      ;[rpcinterface:supervisor]
      ; Sample supervisor config file.
      ;supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

      [supervisorctl]
      serverurl=http://127.0.0.1:9001 ; use an http:// url to specify an inet socket
      ;username=chris              ; should be same as http_username if set
      ;password=123                ; should be same as http_password if set
      ;prompt=mysupervisor         ; cmd line prompt (default "supervisor")
      ;history_file=~/.sc_history  ; use readline history if available

      [supervisord]
      logfile=${c.home}/log/supervisord.log ; (main log file;default $CWD/supervisord.log)
      logfile_maxbytes=50MB        ; (max main logfile bytes b4 rotation;default 50MB)
      logfile_backups=10           ; (num of main logfile rotation backups;default 10)
      loglevel=info                ; (log level;default info; others: debug,warn,trace)
      pidfile=${c.home}/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
      nodaemon=false               ; (start in foreground if true;default false)
      minfds=1024                  ; (min. avail startup file descriptors;default 1024)
      minprocs=200                 ; (min. avail process descriptors;default 200)
      ;user=root

      [include]
      files = ${supervisorProgramms}/etc/supervisor/conf.d/*.conf
    '';
  };

  entrypoint = pkgs.writeTextFile {
    name = "supervisor-nextcloud-conf";
    executable = true;
    destination = "/bin/entrypoint";
    text = ''
      #!${pkgs.bash}/bin/bash

      mkdir -vp ${c.home}/log 
      mkdir -vp ${c.home}/run

      ${supervisord}/bin/supervisord -c ${supervisorConf}/etc/supervisor/supervisor.conf -d

      tail -f ${c.home}/log/*
    '';
  };

  occ = pkgs.writeTextFile {
    name = "supervisor-nextcloud-conf";
    executable = true;
    destination = "/bin/occ";
    text = ''
      #!${pkgs.bash}/bin/bash
      EXTCLOUD_CONFIG_DIR=${c.home}/nextcloud/config php ${c.package}/occ $@
    '';
  };

  start = pkgs.writeTextFile {
    name = "supervisor-nextcloud-conf";
    executable = true;
    destination = "/bin/start";
    text = ''
      #!${pkgs.bash}/bin/bash

      mkdir -vp ${c.home}/log 
      mkdir -vp ${c.home}/run
      
      echo "Starting ${supervisord}/bin/supervisord -c ${supervisorConf}/etc/supervisor/supervisor.conf"
      ${supervisord}/bin/supervisord -c ${supervisorConf}/etc/supervisor/supervisor.conf -d
    '';
  };

  status = pkgs.writeTextFile {
    name = "supervisor-nextcloud-conf";
    executable = true;
    destination = "/bin/status";
    text = ''
      #!${pkgs.bash}/bin/bash
      ${supervisord}/bin/supervisord -c ${supervisorConf}/etc/supervisor/supervisor.conf ctl status -s http://127.0.0.1:9001
    '';
  };

  stop = pkgs.writeTextFile {
    name = "supervisor-nextcloud-conf";
    executable = true;
    destination = "/bin/stop";
    text = ''
      #!${pkgs.bash}/bin/bash
      ${supervisord}/bin/supervisord -c ${supervisorConf}/etc/supervisor/supervisor.conf ctl shutdown -s http://127.0.0.1:9001
    '';
  };

in { inherit entrypoint occ start status stop; }

