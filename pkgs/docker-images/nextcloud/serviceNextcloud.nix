{ pkgs, lib, config, ... }:
let
  inherit (lib) optionalString;

  c = config.services.nextcloud;

  nextcloudConfig = import ./nextcloud-config.nix {
    inherit pkgs;
    inherit lib;
    inherit config;
  };

  prepareScript = pkgs.writeTextFile {
    name = "nextcloud-supervisor-prepare";
    executable = true;
    destination = "/bin/nextcloud-prepare";
    text = ''
      #!${pkgs.bash}/bin/bash

      if [ ! -d ${c.home}/nextcloud/config ]; then
        mkdir -vp ${c.home}/nextcloud/config
      fi
      if [ ! -d ${c.home}/nextcloud/store-apps ]; then
        mkdir -vp ${c.home}/nextcloud/store-apps
      fi
      if [ ! -d ${c.home}/nextcloud/data ]; then
        mkdir -vp ${c.home}/nextcloud/data
      fi
      if [ ! -d ${c.home}/log ]; then
        mkdir -vp ${c.home}/log
      fi
      if [ ! -d ${c.home}/run ]; then
        mkdir -vp ${c.home}/run
      fi
      if [ ! -d ${c.home}/nginx ]; then
        mkdir -vp ${c.home}/nginx
      fi

      ${optionalString (c.instanceid != null)
      "cp -v ${nextcloudConfig}/config/config.php ${c.home}/nextcloud/config/config.php"}
      if [ ! -f ${c.home}/nextcloud/config/config.php ]; then
        cp -v ${nextcloudConfig}/config/config.php ${c.home}/nextcloud/config/config.php
        touch ${c.home}/nextcloud/config/CAN_INSTALL
      fi

      rm ${c.home}/nextcloud/cron.php
      cp -v ${c.package}/cron.php ${c.home}/nextcloud/cron.php

      unlink ${c.home}/nextcloud/apps
      ln -fs ${c.package}/apps ${c.home}/nextcloud/apps

      # if you dont use this php-fpm do an segfault !
      unlink ${c.home}/nextcloud/core
      ln -fs ${c.package}/core ${c.home}/nextcloud/core

      # for cron.php
      unlink ${c.home}/nextcloud/lib
      ln -fs ${c.package}/lib ${c.home}/nextcloud/lib
    '';
  };

  supervisorconf = pkgs.writeTextFile {
    name = "nextcloud-supervisor-copy-nginx";
    executable = false;
    destination = "/etc/supervisor/conf.d/nginx-copy.conf";
    text = ''
      [program:nextcloud-prepare]
      stdout_logfile=${c.home}/log/supervisor_prepare.log
      redirect_stderr=true
      command=${pkgs.bash}/bin/bash ${prepareScript}/bin/nextcloud-prepare
      startsecs=0
      autostart=true
      autorestart=false
    '';
  };

in supervisorconf
