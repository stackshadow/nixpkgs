{ pkgs, lib, config, ... }:
let
  inherit (lib) optionalString;
  c = config.services.nextcloud;

  startPGScript = pkgs.writeTextFile {
    name = "nextcloud-supervisor-psql-start";
    executable = true;
    destination = "/bin/nextcloud-psql";
    text = ''
      #!${pkgs.bash}/bin/bash
      PSQL="${pkgs.postgresql}/bin/psql --port=${
        toString c.dbport
      } -h 127.0.0.1 -U postgres"

      echo "Try to stop existing DB-Server"
      ${pkgs.postgresql}/bin/pg_ctl --wait -D ${c.home}/pdb -o "-k ${c.home}/pdb" stop || true
      echo "Server stopped"

      if [ ! -f ${c.home}/pdb/PG_VERSION ]; then
          mkdir -vp  ${c.home}/pdb

          # Cleanup the data directory.
          rm -f ${c.home}/pdb/*.conf
          
          # Initialise the database.
          echo "Init the DB"
          ${pkgs.postgresql}/bin/initdb -U postgres ${c.home}/pdb
          echo "Init the DB... Done"

          echo "listen_addresses = '*'" >> ${c.home}/pdb/postgresql.conf
      fi

      if [ ! -f ${c.home}/pdb/.init_finished ]; then
        echo "Try to start postgresql"
        ${pkgs.postgresql}/bin/pg_ctl --wait -D ${c.home}/pdb -o "-k ${c.home}/pdb" start
        echo "Server started"

        echo "Create user and db"
        $PSQL -tAc "CREATE USER ${c.dbuser} WITH PASSWORD '${c.dbpass}'"
        $PSQL -tAc "CREATE DATABASE ${c.dbname} OWNER ${c.dbuser} ENCODING 'UNICODE'"
        touch ${c.home}/pdb/.init_finished

        $PSQL -tAc "ALTER USER ${c.dbuser} WITH PASSWORD '${c.dbpass}'"
        $PSQL -tAc "ALTER DATABASE ${c.dbname} OWNER TO ${c.dbuser}"
        $PSQL -tAc "GRANT ALL PRIVILEGES ON DATABASE ${c.dbname} TO ${c.dbuser}"

        ${pkgs.postgresql}/bin/pg_ctl --wait -D ${c.home}/pdb -o "-k ${c.home}/pdb" stop
      fi

      ${pkgs.postgresql}/bin/postgres -D ${c.home}/pdb -k ${c.home}/pdb
    '';
  };

  supervisorconf = pkgs.writeTextFile {
    name = "nextcloud-supervisor-pgsql";
    executable = false;
    destination = "/etc/supervisor/conf.d/psql.conf";
    text = ''
      [program:psql]
      stdout_logfile=${c.home}/log/supervisor_psql.log
      redirect_stderr=true
      command=${pkgs.bash}/bin/bash ${startPGScript}/bin/nextcloud-psql
      startsecs=0
      autostart=true
      autorestart=false
    '';
  };

in supervisorconf
