{ system ? builtins.currentSystem, src ? (builtins.fetchGit {
  url = "https://github.com/fzipp/gocyclo.git";
  ref = "refs/tags/${version}";
}), version ? "v0.3.1", vendorSha256 ? null
, homepage ? "github.com/fzipp/gocyclo/cmd/gocyclo"
, repourl ? "github.com/fzipp/gocyclo/cmd/gocyclo" }:
let
  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;
  inherit (pkgs) buildGoModule fetchFromGitHub;

  binary = buildGoModule {
    pname = "gocyclo";
    inherit version;
    inherit src;

    excludedPackages = "test";
    subPackages = [ "cmd/gocyclo" ];

    inherit vendorSha256;
    runVend = true;

    buildFlagsArray = ''
      -ldflags=
      -w -s
      -X main.Version=${version}
      -X main.Commit=${version}
      -extldflags=-static
    '';

    preBuild = ''
      export CGO_ENABLED=0
      export CFLAGS="-I${pkgs.glibc.dev}/include";
      export LDFLAGS="-L${pkgs.glibc}/lib";
    '';

    #passthru.tests = { inherit (nixosTests) telegraf; };

    meta = with lib; {
      description = "gobadge - go based cli to create badges";
      license = licenses.mit;
      homepage = homepage;
      maintainers = with maintainers; [ stackshadow ];
    };
  };

in binary
